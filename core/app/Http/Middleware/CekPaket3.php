<?php

namespace App\Http\Middleware;

use App\Models\Gold;
use Closure;
use Illuminate\Http\Request;

class CekPaket3
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $paket1 = Gold::where('user_id', '=', auth()->user()->id)->where('prod_id','44')->first();
        if ($paket1) {
            return $next($request);
        }
        $notify[] = ['error','Sorry, You have to purchase paket 1 first.'];
        return redirect()->route('user.home')->withNotify($notify);
    }
}
