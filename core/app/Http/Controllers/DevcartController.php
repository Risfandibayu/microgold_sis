<?php

namespace App\Http\Controllers;

use App\Models\devcart;
use App\Http\Controllers\Controller;
use App\Models\alamat;
use App\Models\Gold;
use App\Models\sendgold;
use App\Models\sgdetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DevcartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        return $this->activeTemplate = activeTemplate();
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\devcart  $devcart
     * @return \Illuminate\Http\Response
     */
    public function show(devcart $devcart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\devcart  $devcart
     * @return \Illuminate\Http\Response
     */
    public function edit(devcart $devcart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\devcart  $devcart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, devcart $devcart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\devcart  $devcart
     * @return \Illuminate\Http\Response
     */
    public function destroy(devcart $devcart)
    {
        //
    }

    public function ind(){
        $page_title = 'Delivery Cart';
        $empty_message = 'Delivery Cart Not found.';
        $devcart = devcart::where('user_id',Auth::user()->id)->paginate(getPaginate());
        $alamat = alamat::where('user_id',Auth::user()->id)->get();

        return view('templates.basic.user.devcart',compact('page_title', 'empty_message','devcart','alamat'));
    }

    public function addToCart(Request $request){
        // dd($request->all());

        $gold = Gold::where('id',$request->gid)->first();

        if ($gold->qty < $request->qty) {
            # code...
            $notify[] = ['error', 'The qty you input exceeds the amount of gold you have'];
            return redirect()->back()->withNotify($notify);
        }

        $fc = devcart::where('gold_id',$request->gid)->first();


        if($fc){
            if ($fc->qty + $request->qty > $gold->qty) {
                # code...
                $notify[] = ['error', 'The qty you input exceeds the amount of gold you have'];
                return redirect()->back()->withNotify($notify);
            }
            $cart = $fc;
            $cart->qty += $request->qty;
        }else{
            $cart = new devcart();
            $cart->qty =  $request->qty;
        }

        $cart->user_id =  Auth::user()->id;
        $cart->gold_id =  $request->gid;
        $cart->save();

        $notify[] = ['success', 'Add To Delivery Cart Succesfully'];
        return back()->withNotify($notify);
    }

    public function delCart(Request $request, $id){
        $cart = devcart::where('id',$request->id)->delete();

        $notify[] = ['success', 'Deleted Succesfully'];
        return back()->withNotify($notify);
    }

    public function updCart(Request $request){
        dd($request->all());
    }

    public function checkOut(Request $request){
        // dd($request->all());


        $user = Auth::user();
        $index = 1;
        $berattotal = 0;
        $alamat = alamat::where('id', $request->alamat)->first();
        
        if (!$alamat->kode_pos) {
            # code...
            $notify[] = ['error', 'Incomplete address, please complete the recipient`s address first'];
            return redirect()->back()->withNotify($notify);
            
        }
        if (!$request->id) {
            # code...
            $notify[] = ['error', 'The Shipping Cart Is Empty'];
            return redirect()->back()->withNotify($notify);
            
        }
        session()->put('Alamat', $alamat);

        foreach($request->id as $id){
            $dcart = devcart::where('id',$id)->first();
            $gold = Gold::where('id',$dcart->gold->id)->first();

            if ($request->quant[$index] > $gold->qty ) {
                # code...
                $notify[] = ['error', 'The qty you input exceeds the amount of gold you have'];
                return redirect()->back()->withNotify($notify);
            }
            $berat = $dcart->gold->prod->weight * $dcart->qty;
                # code...
            $berattotal = $berattotal + $berat;
            
            $index++;   
        }
        session()->put('Gold', $request->id);
        session()->put('Qty', $request->quant);

        
        $options = [
            'cache_wsdl'     => WSDL_CACHE_NONE,
            'trace'          => 1,
            'stream_context' => stream_context_create(
                [
                    'ssl' => [
                        'verify_peer'       => false,
                        'verify_peer_name'  => false,
                        'allow_self_signed' => true
                    ]
                ]
            )
        ];

        try {
        $wsdl="http://api.rpxholding.com/wsdl/rpxwsdl.php?wsdl";
        libxml_disable_entity_loader(false);
        $client = new \SoapClient($wsdl,$options);
        $username = env('USER_RPX');
        $password  = env('PASS_RPX');
        $format = 'json';

        
            $result = $client->getRatesPostalCode($username,$password,env('POSCODE_RPX'),$alamat->kode_pos,'RGP',$berattotal/1000,'0',$format);
            $response = json_decode($result,true);
            // dd($response);
            if (!$response) {
                # code...
                $notify[] = ['error', 'Invalid destination address'];
                return redirect()->back()->withNotify($notify);
            }else{
                // dd((int)$response['RPX']['DATA']['PRICE'] - 20000);
                // if ((int)$response['RPX']['DATA']['PRICE'] > 20000) {
                //     # code...
                    // $ongkir = (int)$response['RPX']['DATA']['PRICE'] - 20000;
                    // $ongkir = (int)$response['RPX']['DATA']['PRICE'];
                // }else{
                    // $ongkir = 0;
                    
                // }
                if ($alamat->kode_pos == env('POSCODE_RPX')) {
                    # code...
                    $ongkir = 0;
                }else{
                    $ongkir = (int)$response['RPX']['DATA']['PRICE'];
                }

                // if($user->balance < $ongkir){
                //     $notify[] = ['error', 'Your balance is not enough for postage payment'];
                //     return redirect()->back()->withNotify($notify);
                // }else{

                    session()->put('Ongkir', $ongkir);
                    // $notify[] = ['success', 'Gold delivery request is successful, please wait for confirmation.'];
                    // return redirect()->back()->withNotify($notify);
                // }
            }
        }
        catch ( \Exception $e ) {
            // echo $e->getMessage();
            $notify[] = ['error', $e->getMessage()];
            return redirect()->back()->withNotify($notify);
        }
        // dd($ongkir);

        return redirect()->route('user.sg.preview');

    }
    public function checkOutPreview(){
        // dd(session()->get('Qty'));
        $qty = session()->get('Qty');
        $index = 1;
        $berattotal = 0;
        $keptotal = 0;

        $alamat = session()->get('Alamat');
        $ongkir = session()->get('Ongkir');
        // dd($qty[1]);
        $page_title = 'Delivery Gold Preview';

        foreach(session()->get('Gold') as $id){
            $dc= devcart::where('id',$id)->first();
            if($qty[$index] != $dc->qty){
                $dc->qty = $qty[$index];
                $dc->save();
            }

            $berat = $dc->gold->prod->weight * $dc->qty;
                # code...
            $berattotal = $berattotal + $berat;
            $keping = $dc->qty;
            $keptotal = $keptotal + $keping;
            $index++;  
        }
        // dd($keptotal);
        

        foreach(session()->get('Gold') as $id){
            $devcart[] = devcart::where('id',$id)->first();
            // $gold[] = Gold::where('id',$dcart->gold->id)->first();  
        }

        
        // dd($alamat);
        return view($this->activeTemplate . 'user.sgpreview', compact('page_title','devcart','berattotal','keptotal','alamat','ongkir'));
    }

    public function checkOutConfirm(Request $request){
        $user = Auth::user();
        $alamat = alamat::where('id', $request->alamat)->first();
        $sg = new sendgold();
        $sg->trx = getTrx();
        $sg->user_id = $user->id;
        $sg->alamat = $alamat->alamat;
        $sg->nama_penerima = $alamat->nama_penerima;
        $sg->no_telp_penerima = $alamat->no_telp;
        $sg->kode_pos = $alamat->kode_pos;
        $sg->status = 2;
        $sg->ongkir = $request->ongkir;
        $sg->save();

        if($user->balance < $request->ongkir){
            $notify[] = ['error', 'Your balance is not enough for shipping costs payment. You must provide a balance of Rp. '.$request->ongkir.' for shipping costs'];
            return redirect()->back()->withNotify($notify);
        }
        $user->balance -= $sg->ongkir;
        $user->save();

        $trx = $user->transactions()->create([
            'amount' => $sg->ongkir,
            'trx_type' => '-',
            'details' => 'Payment for shipping costs',
            'remark' => '',
            'trx' => getTrx(),
            'post_balance' => getAmount($user->balance),
        ]);

        $devcart = devcart::where('user_id',$user->id)->get();

        if (count($devcart) == 0) {
            # code...
            $notify[] = ['error', 'Your delivery cart is empty. Please add product to cart first!!'];
            return redirect()->route('user.gold.invest')->withNotify($notify);
        }

        foreach ($devcart as $item) {
            $sgdet = new sgdetail();
            $sgdet->sg_id = $sg->id;
            $sgdet->gold_id = $item->gold_id;
            $sgdet->qty = $item->qty;
            $sgdet->save();

            $gold = Gold::where('id',$item->gold_id)->first();
            $gold->qty -= $item->qty;
            $gold->save();
        }

        $devcart->each->delete();



        // dd($sg);
        $notifyl[] = ['success', 'Gold delivery request is successful, please wait for confirmation. Please contact admin for further shipping info','Chat Admin','https://wa.me/6281282174631?text=Hallo%20admin%2C%20%0AMinta%20informasi%20tentang%20delivery%20Gold%20dengan%20ID%20%3A%20'.$sg->trx];
        return redirect()->route('user.report.deliveryLog')->withNotifyl($notifyl);
    }

}
