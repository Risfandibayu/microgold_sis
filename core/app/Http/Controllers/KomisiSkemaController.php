<?php

namespace App\Http\Controllers;

use App\Models\komisi_skema;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KomisiSkemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\komisi_skema  $komisi_skema
     * @return \Illuminate\Http\Response
     */
    public function show(komisi_skema $komisi_skema)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\komisi_skema  $komisi_skema
     * @return \Illuminate\Http\Response
     */
    public function edit(komisi_skema $komisi_skema)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\komisi_skema  $komisi_skema
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, komisi_skema $komisi_skema)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\komisi_skema  $komisi_skema
     * @return \Illuminate\Http\Response
     */
    public function destroy(komisi_skema $komisi_skema)
    {
        //
    }
}
