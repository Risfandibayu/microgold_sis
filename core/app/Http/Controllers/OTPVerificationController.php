<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\OTPVerification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OTPVerificationController extends Controller
{
    //
    public function sendOTP()
{
    $user = Auth::user();
    $otp = rand(100000, 999999);

    $otpVerification = new OTPVerification();
    $otpVerification->user_id = $user->id;
    $otpVerification->otp = $otp;
    $otpVerification->save();

    // Kode untuk mengirimkan OTP ke pengguna (misalnya melalui SMS atau email)
    // Contoh: Kirim OTP melalui email
    // Mail::to($user->email)->send(new OTPMail($otp));
    sendEmail($user, 'EVER_CODE_PRODUCT',[
        'code' => $otp
    ]);

    return response()->json(['message' => 'OTP sent successfully, Please check your inbox or spam folder in your email and enter the OTP code in the form above']);
}
}
