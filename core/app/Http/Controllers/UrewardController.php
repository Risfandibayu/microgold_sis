<?php

namespace App\Http\Controllers;

use App\Models\ureward;
use App\Http\Controllers\Controller;
use App\Models\bonus_reward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UrewardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ureward  $ureward
     * @return \Illuminate\Http\Response
     */
    public function show(ureward $ureward)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ureward  $ureward
     * @return \Illuminate\Http\Response
     */
    public function edit(ureward $ureward)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ureward  $ureward
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ureward $ureward)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ureward  $ureward
     * @return \Illuminate\Http\Response
     */
    public function destroy(ureward $ureward)
    {
        //
    }
    public function claimReward(Request $request,$id){
        // dd($request->all());
        $rew = bonus_reward::find($id);
        $weak = auth()->user()->userExtra->left < auth()->user()->userExtra->right ? auth()->user()->userExtra->left : auth()->user()->userExtra->right;
        if(intval($weak/$rew->kiri) < $request->qty){
            $notify[] = ['error', 'Claim reward qty exceeds the existing amount'];
            return redirect()->back()->withNotify($notify);
        }

        DB::beginTransaction();

        try {
            for ($x = 1; $x <= $request->qty; $x++) {
                // echo "The number is: $x <br>";
                $cr = new ureward();
                $cr->trx = getTrx();
                $cr->user_id = Auth::user()->id;
                $cr->reward_id = $rew->id;
                $cr->save();
            }

            $user = auth()->user();
            $user->reward_claim += $request->qty*$rew->kiri;
            $user->save();

            DB::table('urewardqty')->insert(
                [
                    'user_id' => Auth::user()->id,
                    'reward_id' => $rew->id,
                    'qty'=>$request->qty,
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                    ]
            );
            DB::commit();
            $notify[] = ['success', 'Reward successfully claimed!!'];
            return redirect()->back()->withNotify($notify);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            $notify[] = ['error', 'Claim failed, please try again.'.$e];
            return redirect()->back()->withNotify($notify);
        }

        
    }
    public function claimRewardUp(Request $request,$id){
        // dd($request->all());
        $rew = bonus_reward::find($id);
        $weak = auth()->user()->userExtra->left < auth()->user()->userExtra->right ? auth()->user()->userExtra->left : auth()->user()->userExtra->right;
        if(intval($weak/$rew->kiri) - cekRewardClaimed($rew->id) < $request->qty){
            $notify[] = ['error', 'Claim reward qty exceeds the existing amount'];
            return redirect()->back()->withNotify($notify);
        }
        DB::beginTransaction();

        try {
            for ($x = 1; $x <= $request->qty; $x++) {
                // echo "The number is: $x <br>";
                $cr = new ureward();
                $cr->trx = getTrx();
                $cr->user_id = Auth::user()->id;
                $cr->reward_id = $rew->id;
                $cr->save();
            }

            $user = auth()->user();
            $user->reward_claim += ($request->qty*$rew->kiri);
            $user->save();

            DB::table('urewardqty')->where('user_id',Auth::user()->id)->where('reward_id', $id)->update(
                [
                    'user_id' => Auth::user()->id,
                    'qty'=>DB::raw("qty + $request->qty"),
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                    ]
            );
            DB::commit();
            $notify[] = ['success', 'Reward successfully claimed!!'];
            return redirect()->back()->withNotify($notify);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            $notify[] = ['error', 'Claim failed, please try again.'.$e];
            return redirect()->back()->withNotify($notify);
        }

        
    }

    public function userReward(){
        $page_title = 'Bonus Reward';
        $empty_message = 'Bonus Reward Not found.';
        $reward = ureward::where('user_id',Auth::user()->id)->paginate(getPaginate());
        // dd($reward);

        return view('templates.basic.user.reward',compact('page_title', 'empty_message','reward'));
    }

    public function printTicket($trx){

        $pageTitle = "Ticket Print";
        $ticket = ureward::where('trx',$trx)->where('user_id',Auth::user()->id)->first();
        if (!$ticket) {
            # code...
            return redirect()->back();
        }
        return view('templates.basic.user.print_ticket', compact('ticket', 'pageTitle'));
    }
}
