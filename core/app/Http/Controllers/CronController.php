<?php

namespace App\Http\Controllers;

use App\Models\GeneralSetting;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserExtra;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CronController extends Controller
{
    // public function cron()
    // {
    //     $gnl = GeneralSetting::first();
    //     $gnl->last_cron = Carbon::now()->toDateTimeString();
	// 	$gnl->save();

    //     if ($gnl->matching_bonus_time == 'daily') {
    //         $day = Date('H');
    //         if (strtolower($day) != $gnl->matching_when) {
    //             return '1';
    //         }
    //     }

    //     if ($gnl->matching_bonus_time == 'weekly') {
    //         $day = Date('D');
    //         if (strtolower($day) != $gnl->matching_when) {
    //             return '2';
    //         }
    //     }

    //     if ($gnl->matching_bonus_time == 'monthly') {
    //         $day = Date('d');
    //         if (strtolower($day) != $gnl->matching_when) {
    //             return '3';
    //         }
    //     }

    //     if (Carbon::now()->toDateString() == Carbon::parse($gnl->last_paid)->toDateString()) {
    //         /////// bv done for today '------'
    //         ///////////////////LETS PAY THE BONUS

    //         $gnl->last_paid = Carbon::now()->toDateString();
    //         $gnl->save();

    //         $eligibleUsers = UserExtra::where('bv_left', '>=', $gnl->total_bv)->where('bv_right', '>=', $gnl->total_bv)->get();

    //         foreach ($eligibleUsers as $uex) {
                // $user = $uex->user;
    //             $weak = $uex->bv_left < $uex->bv_right ? $uex->bv_left : $uex->bv_right;
    //             $weaker = $weak < $gnl->max_bv ? $weak : $gnl->max_bv;

    //             $pair = intval($weaker / $gnl->total_bv);

    //             $bonus = $pair * $gnl->bv_price;

    //             // add balance to User

    //             $payment = User::find($uex->user_id);
    //             $payment->balance += $bonus;
    //             $payment->save();

    //             $trx = new Transaction();
    //             $trx->user_id = $payment->id;
    //             $trx->amount = $bonus;
    //             $trx->charge = 0;
    //             $trx->trx_type = '+';
    //             $trx->post_balance = $payment->balance;
    //             $trx->remark = 'binary_commission';
    //             $trx->trx = getTrx();
    //             $trx->details = 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $pair * $gnl->total_bv . ' BV.';
    //             $trx->save();

                // notify($user, 'matching_bonus', [
                //     'amount' => $bonus,
                //     'currency' => $gnl->cur_text,
                //     'paid_bv' => $pair * $gnl->total_bv,
                //     'post_balance' => $payment->balance,
                //     'trx' =>  $trx->trx,
                // ]);

    //             $paidbv = $pair * $gnl->total_bv;
    //             if ($gnl->cary_flash == 0) {
    //                 $bv['setl'] = $uex->bv_left - $paidbv;
    //                 $bv['setr'] = $uex->bv_right - $paidbv;
    //                 $bv['paid'] = $paidbv;
    //                 $bv['lostl'] = 0;
    //                 $bv['lostr'] = 0;
    //             }
    //             if ($gnl->cary_flash == 1) {
    //                 $bv['setl'] = $uex->bv_left - $weak;
    //                 $bv['setr'] = $uex->bv_right - $weak;
    //                 $bv['paid'] = $paidbv;
    //                 $bv['lostl'] = $weak - $paidbv;
    //                 $bv['lostr'] = $weak - $paidbv;
    //             }
    //             if ($gnl->cary_flash == 2) {
    //                 $bv['setl'] = 0;
    //                 $bv['setr'] = 0;
    //                 $bv['paid'] = $paidbv;
    //                 $bv['lostl'] = $uex->bv_left - $paidbv;
    //                 $bv['lostr'] = $uex->bv_right - $paidbv;
    //             }
    //             $uex->bv_left = $bv['setl'];
    //             $uex->bv_right = $bv['setr'];
    //             $uex->save();


    //             if ($bv['paid'] != 0) {
    //                 createBVLog($user->id, 1, $bv['paid'], 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $paidbv . ' BV.');
    //                 createBVLog($user->id, 2, $bv['paid'], 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $paidbv . ' BV.');
    //             }
    //             if ($bv['lostl'] != 0) {
    //                 createBVLog($user->id, 1, $bv['lostl'], 'Flush ' . $bv['lostl'] . ' BV after Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $paidbv . ' BV.');
    //             }
    //             if ($bv['lostr'] != 0) {
    //                 createBVLog($user->id, 2, $bv['lostr'], 'Flush ' . $bv['lostr'] . ' BV after Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $paidbv . ' BV.');
    //             }
    //         }
    //         return '---';
    //     }
    // }
    public function cron()
    {
        $gnl = GeneralSetting::first();
        $gnl->last_cron = Carbon::now()->toDateTimeString();
		$gnl->save();
        // dd(Date('H:i') == "14:57");
        $userx = UserExtra::where('paid_left','>=',3)
        ->where('paid_right','>=',3)->get();

        // dd($userx);
        $cron = array();
        foreach ($userx as $uex) {
                        $user = $uex->user_id;
                        $weak = $uex->paid_left < $uex->paid_right ? $uex->paid_left : $uex->paid_right;
                        // $weaker = $weak < $gnl->max_bv ? $weak : $gnl->max_bv;
                        $total = $uex->paid_left > $uex->paid_right ? $uex->paid_left : $uex->paid_right;
                        $stotal = $uex->left > $uex->right ? $uex->left : $uex->right;
                        $uex->st_leg = $total;
                        // $uex->sleg = $stotal;
                        $uex->save();

                        

                        $user_plan = user::where('users.id',$user)
                        ->join('plans','plans.id','=','users.plan_id')->first();                        
                        if (!$user_plan) {
                            # code...
                            continue;
                        }
                        $pairs = intval($weak / 3);
                        $pair = intval($weak / 3);

                        if($uex->level_binary != 0 && $pairs != $uex->level_binary){
                            // $pair = intval($weak / 3) - $uex->level_binary;
                            if ($pair > $uex->level_binary) {
                                if ($pair - $uex->level_binary >= 10) {
                                    # code...
                                        if ($pair >= 10) {
                                            $pair = 10;
                                            $bonus = intval(($pair - $uex->level_binary) * ($user_plan->tree_com * 6));
                                        }else{
                                            $bonus = intval(($pair - $uex->level_binary) * ($user_plan->tree_com * 6));
                                        }
                                }else{

                                    if ($pair >= 10) {
                                        $pair = 10;
                                        $bonus = intval(($pair - $uex->level_binary) * ($user_plan->tree_com * 6));
                                    }else{
                                        $bonus = intval(($pair - $uex->level_binary) * ($user_plan->tree_com * 6));
                                    }
                                }

                            }else{
                                if ($pair >= 10) {
                                    $pair = 10;
                                    $bonus = intval(($uex->level_binary - $pair ) * ($user_plan->tree_com * 6));
                                }else{
                                    $bonus = intval(($uex->level_binary - $pair ) * ($user_plan->tree_com * 6));
                                }
                            }
                        }else{
                            if ($pair >= 10) {
                                # code...
                                $pair = 10;
                                $bonus = intval($pair * ($user_plan->tree_com * 6));
                            }else{
                                $bonus = intval($pair * ($user_plan->tree_com * 6));
                            }
                        }

                        $pair2[] = $pair == $uex->level_binary;

                        if ($pair >= 10) {
                            $pair = 10;
                        }

                        // if($uex->level_binary != 0 && $pairs != $uex->level_binary){
                        //     // $pair = intval($weak / 3) - $uex->level_binary;
                        //     if ($pair > $uex->level_binary) {
                        //         $bonus = intval(($pair - $uex->level_binary) * ($user_plan->tree_com * 6));
                        //     }else{
                        //         $bonus = intval(($uex->level_binary - $pair ) * ($user_plan->tree_com * 6));
                        //     }
                        // }else{
                        //     $bonus = intval($pair * ($user_plan->tree_com * 6));
                        // }
                        // $bonus = intval($pair * ($user_plan->tree_com * 6));

                        // dd(is_numeric($uex->paid_left));


                        if ($pair == $uex->level_binary) {
                            // if ($uex->level_binary == 10) {
                            //     $payment = User::find($uex->user_id);
                            //     $payment->balance += $bonus;
                            //     $payment->save();
    
                            //     $trx = new Transaction();
                            //     $trx->user_id = $payment->id;
                            //     $trx->amount = $bonus;
                            //     $trx->charge = 0;
                            //     $trx->trx_type = '+';
                            //     $trx->post_balance = $payment->balance;
                            //     $trx->remark = 'binary_commission';
                            //     $trx->trx = getTrx();
                            //     $trx->details = 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $pair * 6 . ' BRO.';
                            //     $trx->save();

                            //     $uex->paid_left = 0;
                            //     $uex->paid_right = 0;
                            //     $uex->save();
    
                            //     sendEmail2($user, 'matching_bonus', [
                            //             'amount' => $bonus,
                            //             'currency' => $gnl->cur_text,
                            //             'paid_bv' => $pair * 6,
                            //             'post_balance' => $payment->balance,
                            //             'trx' =>  $trx->trx,
                            //     ]);
                            // }else{

                            // }

                        }else{
                            $payment = User::find($uex->user_id);
                            $payment->balance += $bonus;
                            $payment->total_binary_com += $bonus;
                            

                            $trx = new Transaction();
                            $trx->user_id = $payment->id;
                            $trx->amount = $bonus;
                            $trx->charge = 0;
                            $trx->trx_type = '+';
                            $trx->post_balance = $payment->balance;
                            $trx->remark = 'binary_commission';
                            $trx->trx = getTrx();

                            if ($pair >= 10) {
                                if (Date('H') == "01" ) {
                                    if ($uex->last_flush_out) {
                                        if (Carbon::parse($uex->last_flush_out)->format('Y-m-d') != Carbon::now()->toDateString()) {
                                        # code...

                                            $paid_bv = $uex->paid_left + $uex->paid_right;
                                        // }else{
                                        // }
                                        
                                            // sendEmail2($user, 'matching_bonus', [
                                            //         'amount' => $bonus,
                                            //         'currency' => $gnl->cur_text,
                                            //         'paid_bv' => $paid_bv,
                                            //         'post_balance' => $payment->balance,
                                            //         'trx' =>  $trx->trx,
                                            // ]);
                                        
                                            if($uex->level_binary == 0){
                                                $payment->save();
                                                $trx->details = 'Paid Flush Out ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $pair * 6 . ' BRO.';
                                            // }else{
                                            //     $trx->details = 'Paid Flush Out ' . $bonus . ' ' . $gnl->cur_text . ' For ' . ($pair-$uex->level_binary) * 6 . ' BRO.';
                                            // }

                                        // }
                                                $trx->save();
                                                
                                                // $uex->paid_left = 0;
                                                // $uex->paid_right = 0;
                                                $uex->paid_left -= 30;
                                                $uex->paid_right -= 30;
                                                $uex->level_binary = 0;
                                                // $uex->last_flush_out = Carbon::now()->toDateTimeString();
                                                $uex->save();

                                                $gnl->last_paid = Carbon::now()->toDateTimeString();
                                                $gnl->save();

                                                // Carbon::now()->toDateString()
                                                $cron[] = $user.'/'.$pair.'/'.Carbon::parse($uex->last_flush_out)->format('Y-m-d').'/FlushOut2';
                                                
                                            }else{

                                                if (strtolower(Date('D')) == 'wed'){
                                                
                                                    $payment->save();
                                                    $trx->details = 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . ($pair-$uex->level_binary) * 6 . ' BRO.';

                                                    $trx->save();
                                                
                                                    $uex->paid_left -= 30;
                                                    $uex->paid_right -= 30;
                                                    $uex->level_binary = 0;
                                                    // $uex->last_flush_out = Carbon::now()->toDateTimeString();
                                                    $uex->save();
        
                                                    $gnl->last_paid = Carbon::now()->toDateTimeString();
                                                    $gnl->save();
        
                                                    // Carbon::now()->toDateString()
                                                    $cron[] = $user.'/'.$pair.'/'.Carbon::parse($uex->last_flush_out)->format('Y-m-d');
                                                }
                                            }
                                        }else{
                                            if (strtolower(Date('D')) == 'wed'){

                                        
                                        # code...

                                                $paid_bv = $uex->paid_left + $uex->paid_right;
                                                // }else{
                                                // }
                                                
                                                    // sendEmail2($user, 'matching_bonus', [
                                                    //         'amount' => $bonus,
                                                    //         'currency' => $gnl->cur_text,
                                                    //         'paid_bv' => $paid_bv,
                                                    //         'post_balance' => $payment->balance,
                                                    //         'trx' =>  $trx->trx,
                                                    // ]);
                                                
                                                // if ($pair >= 10) {
                                                $payment->save();

                                                if($uex->level_binary == 0){
                                                    $trx->details = 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $pair * 6 . ' BRO.';
                                                }else{
                                                    $trx->details = 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . ($pair-$uex->level_binary) * 6 . ' BRO.';
                                                }
                                                $trx->save();
                                                
                                                $uex->paid_left -= 30;
                                                $uex->paid_right -= 30;
                                                $uex->level_binary = 0;
                                                // $uex->last_flush_out = Carbon::now()->toDateTimeString();
                                                $uex->save();

                                                $gnl->last_paid = Carbon::now()->toDateTimeString();
                                                $gnl->save();

                                                // Carbon::now()->toDateString()
                                                $cron[] = $user.'/'.$pair.'/'.Carbon::parse($uex->last_flush_out)->format('Y-m-d');
                                            }
                                        }
                                    }else{

                                        
                                        # code...

                                            $paid_bv = $uex->paid_left + $uex->paid_right;
                                            // }else{
                                            // }
                                            
                                                // sendEmail2($user, 'matching_bonus', [
                                                //         'amount' => $bonus,
                                                //         'currency' => $gnl->cur_text,
                                                //         'paid_bv' => $paid_bv,
                                                //         'post_balance' => $payment->balance,
                                                //         'trx' =>  $trx->trx,
                                                // ]);
                                            
                                            // if ($pair >= 10) {
                                            

                                                if($uex->level_binary == 0){
                                                    $payment->save();
                                                    $trx->details = 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $pair * 6 . ' BRO.';
                                                // }else{
                                                //     $trx->details = 'Paid Flush Out ' . $bonus . ' ' . $gnl->cur_text . ' For ' . ($pair-$uex->level_binary) * 6 . ' BRO.';
                                                // }

                                            // }
                                                    $trx->save();
                                                    
                                                    $uex->paid_left -= 30;
                                                    $uex->paid_right -= 30;
                                                    $uex->level_binary = 0;
                                                    $uex->last_flush_out = Carbon::now()->toDateTimeString();
                                                    $uex->save();

                                                    $gnl->last_paid = Carbon::now()->toDateTimeString();
                                                    $gnl->save();

                                                    // Carbon::now()->toDateString()
                                                    $cron[] = $user.'/'.$pair.'/'.Carbon::parse($uex->last_flush_out)->format('Y-m-d').'/FlushOut1';
                                                    
                                                }else{
                                                    if (strtolower(Date('D')) == 'wed'){
                                                        $payment->save();
                                                        $trx->details = 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . ($pair-$uex->level_binary) * 6 . ' BRO.';

                                                        $trx->save();
                                                    
                                                        $uex->paid_left -= 30;
                                                        $uex->paid_right -= 30;
                                                        $uex->level_binary = 0;
                                                        // $uex->last_flush_out = Carbon::now()->toDateTimeString();
                                                        $uex->save();
            
                                                        $gnl->last_paid = Carbon::now()->toDateTimeString();
                                                        $gnl->save();
            
                                                        // Carbon::now()->toDateString()
                                                        $cron[] = $user.'/'.$pair.'/'.Carbon::parse($uex->last_flush_out)->format('Y-m-d');
                                                    }
                                                }
                                                
                                    }
                                }



                            }else{
                                if (strtolower(Date('D')) == 'wed' && Date('H') == "01" ) {
                                    # code...
                                
                                $paid_bv = $pair * 6;
                                // sendEmail2($user, 'matching_bonus', [
                                //     'amount' => $bonus,
                                //     'currency' => $gnl->cur_text,
                                //     'paid_bv' => $paid_bv,
                                //     'post_balance' => $payment->balance,
                                //     'trx' =>  $trx->trx,
                                // ]);
                                $payment->save();

                                    if($uex->level_binary != 0 && $pairs != $uex->level_binary){
                                        $trx->details = 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . ($pair-$uex->level_binary) * 6 . ' BRO.';
                                    }else{
                                        $trx->details = 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $pair * 6 . ' BRO.';

                                    }
                                $trx->save();

                                $uex->level_binary = $pair;
                                $uex->save();

                                $gnl->last_paid = Carbon::now()->toDateTimeString();
                                $gnl->save();

                                $cron[] = $user.'/'.$pair;
                                }else{
                                }
                            }

                            
                        }
        }
        return $cron;
        // dd($dd);

    }   

    public function cronsave(){
        $gnl = GeneralSetting::first();
        $gnl->last_cron = Carbon::now()->toDateTimeString();
		$gnl->save();
        // dd(Date('H:i') == "14:57");
        $userx = UserExtra::where('left','>=',51)
        ->where('right','>=',51)->get();

        $cron = array();
        foreach ($userx as $uex) {
            $total = $uex->left > $uex->right ? $uex->left : $uex->right;
            $weak = $uex->left < $uex->right ? $uex->left : $uex->right;
            // $total = $uex->st_leg;
            $level = $uex->level_binary;
            // $levels = $uex->level_saving;
            if ($uex->sleg != 0) {
                # code...
                $save = $total - $uex->sleg;
            }else{
                $save = $total - ( intval($weak/3) * 3);
            }
            $sl = (intval(($total - ( intval($weak/3) * 3))/10) * 10) + ( intval($weak/3) * 3);


            $pair = intval($save / 10);
            $bonus = $pair * 10 * 25000;
            // $cron[] = $uex->user_id.'/l='.$uex->paid_left.'/r='.$uex->paid_right.'/t='.$total.'/lvl='.$level.'/s='.$save;

            $payment = User::find($uex->user_id);
            $payment->balance += $bonus;
            $payment->total_binary_com += $bonus;
            

            $trx = new Transaction();
            $trx->user_id = $payment->id;
            $trx->amount = $bonus;
            $trx->charge = 0;
            $trx->trx_type = '+';
            $trx->post_balance = $payment->balance;
            $trx->remark = 'binary_commission';
            $trx->trx = getTrx();                            
            
            if ($pair > 0) {
                if (strtolower(Date('D')) == 'wed' && Date('H') == "02" ) {
                    if ($uex->last_saving_paid) {
                        # code...
                        if (Carbon::parse($uex->last_saving_paid)->format('Y-m-d') != Carbon::now()->toDateString()) {
                            # code...
                            $payment->save();
                            $trx->details = 'Paid Savings Commission ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $pair * 10 . ' BRO.';
                            $trx->save(); 
                            
                            $uex->last_saving_paid = Carbon::now()->toDateTimeString();
                            $uex->level_saving = $level;
                            $uex->sleg = $sl;
                            $uex->save();

                            $gnl->last_paid = Carbon::now()->toDateTimeString();
                            $gnl->save();

                            $cron[] = $uex->user_id.'/'.$save.'/'.$pair;
                        }
                    }else{
                            $payment->save();
                            $trx->details = 'Paid Savings Commission ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $pair * 10 . ' BRO.';
                            $trx->save(); 
                            
                            $uex->last_saving_paid = Carbon::now()->toDateTimeString();
                            $uex->level_saving = $level;
                            $uex->sleg = $sl;
                            $uex->save();

                            $gnl->last_paid = Carbon::now()->toDateTimeString();
                            $gnl->save();

                            $cron[] = $uex->user_id.'/'.$save.'/'.$pair;
                    }
                    
                }
            }

        }

        return $cron;
    }

    public function balanceOnHold(){
        $user = user::where('balance_on_hold','!=',0)->get();
        $cron = array();
        foreach($user as $us){
            if (strtolower(Date('D')) == 'wed' && Date('H') == "02" ) {
                

                $trx = $us->transactions()->create([
                    'amount' => $us->balance_on_hold,
                    'charge' => 0,
                    'trx_type' => '+',
                    'details' => 'Release Balance On Hold',
                    'remark' => '',
                    'trx' => getTrx(),
                    'post_balance' => getAmount($us->balance + $us->balance_on_hold),
                    'post_balance_on_hold' => getAmount(0),

                ]);

                $us->balance += $us->balance_on_hold;
                $us->balance_on_hold = 0;
                $us->save();
                
                $cron[] = $us->id.'/'.$us->balance;
            }
        }
        return $cron;
    }

    // public function cronsave(){
    //     $gnl = GeneralSetting::first();
    //     $gnl->last_cron = Carbon::now()->toDateTimeString();
	// 	$gnl->save();
    //     // dd(Date('H:i') == "14:57");
    //     $userx = UserExtra::where('left','>=',51)
    //     ->where('right','>=',51)->get();

    //     $cron = array();
    //     foreach ($userx as $uex) {
    //         // $total = $uex->paid_left > $uex->paid_right ? $uex->paid_left : $uex->paid_right;
    //         $total = $uex->st_leg;
    //         $level = $uex->level_binary;
    //         $levels = $uex->level_saving;
    //         $save = $total - ($level * 3);

    //         $pair = intval($save / 10);
    //         $bonus = $pair * 10 * 25000;
    //         // $cron[] = $uex->user_id.'/l='.$uex->paid_left.'/r='.$uex->paid_right.'/t='.$total.'/lvl='.$level.'/s='.$save;

    //         $payment = User::find($uex->user_id);
    //         $payment->balance += $bonus;
    //         $payment->total_binary_com += $bonus;
            

    //         $trx = new Transaction();
    //         $trx->user_id = $payment->id;
    //         $trx->amount = $bonus;
    //         $trx->charge = 0;
    //         $trx->trx_type = '+';
    //         $trx->post_balance = $payment->balance;
    //         $trx->remark = 'binary_commission';
    //         $trx->trx = getTrx();                            
            
    //         if ($pair > 0) {
    //             if (strtolower(Date('D')) == 'fri' && Date('H') == "19" ) {
    //                 if ($uex->last_saving_paid) {
    //                     # code...
    //                     if (Carbon::parse($uex->last_saving_paid)->format('Y-m-d') != Carbon::now()->toDateString() && $uex->level_saving != $level) {
    //                         # code...
    //                         $payment->save();
    //                         $trx->details = 'Paid Savings Commission ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $save . ' BRO.';
    //                         $trx->save(); 
                            
    //                         $uex->last_saving_paid = Carbon::now()->toDateTimeString();
    //                         $uex->level_saving = $level;
    //                         $uex->save();

    //                         $gnl->last_paid = Carbon::now()->toDateTimeString();
    //                         $gnl->save();

    //                         $cron[] = $uex->user_id.'/'.$save.'/'.$pair;
    //                     }
    //                 }else{
    //                         $payment->save();
    //                         $trx->details = 'Paid Savings Commission ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $save . ' BRO.';
    //                         $trx->save(); 
                            
    //                         $uex->last_saving_paid = Carbon::now()->toDateTimeString();
    //                         $uex->level_saving = $level;
    //                         $uex->save();

    //                         $gnl->last_paid = Carbon::now()->toDateTimeString();
    //                         $gnl->save();

    //                         $cron[] = $uex->user_id.'/'.$save.'/'.$pair;
    //                 }
                    
    //             }
    //         }

    //     }

    //     return $cron;
    // }


    // public function cron30bro()
    // {
    //     $gnl = GeneralSetting::first();
    //     $gnl->last_cron = Carbon::now()->toDateTimeString();
	// 	$gnl->save();

    //     $userx = UserExtra::whereBetween('paid_left','>=',30)
    //     ->whereBetween('paid_right','>=',30)->get();
    //     foreach ($userx as $uex) {
    //                     $user = $uex->user_id;
    //                     $weak = $uex->paid_left < $uex->paid_right ? $uex->paid_left : $uex->paid_right;
    //                     // $weaker = $weak < $gnl->max_bv ? $weak : $gnl->max_bv;
    //                     $user_plan = user::where('users.id',$user)
    //                     ->join('plans','plans.id','=','users.plan_id')->first();                        
    //                     $pair = intval($weak / 3);
    //                     $bonus = intval($pair * ($user_plan->tree_com * 6));

    //                         $payment = User::find($uex->user_id);
    //                         $payment->balance += $bonus;
    //                         $payment->save();

    //                         $trx = new Transaction();
    //                         $trx->user_id = $payment->id;
    //                         $trx->amount = $bonus;
    //                         $trx->charge = 0;
    //                         $trx->trx_type = '+';
    //                         $trx->post_balance = $payment->balance;
    //                         $trx->remark = 'binary_commission';
    //                         $trx->trx = getTrx();
    //                         $trx->details = 'Paid ' . $bonus . ' ' . $gnl->cur_text . ' For ' . $pair * 6 . ' BRO.';
    //                         $trx->save();

    //                         $uex->level_binary = $pair;
    //                         $uex->save();

    //                         sendEmail2($user, 'matching_bonus', [
    //                                 'amount' => $bonus,
    //                                 'currency' => $gnl->cur_text,
    //                                 'paid_bv' => $pair * 6,
    //                                 'post_balance' => $payment->balance,
    //                                 'trx' =>  $trx->trx,
    //                             ]);
    //     }
    //     // dd($dd);
    // }
}
