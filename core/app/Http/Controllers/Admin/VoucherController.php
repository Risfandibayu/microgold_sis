<?php

namespace App\Http\Controllers\Admin;

use App\Models\voucher;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $page_title = 'Vouchers';
        $empty_message = 'No Vouchers found';
        $vouchers = voucher::where('status','!=',1)->paginate(getPaginate());
        return view('admin.voucher.index', compact('page_title','vouchers', 'empty_message'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $v = new voucher();
        $v->name = $request->name;  
        $v->kode_voucher = $request->code ? $request->code : voucherGen();  
        $v->persen = $request->persen ? $request->persen : 0;  
        $v->nominal = $request->nominal ? $request->nominal : 0;  
        $v->for = $request->for ? 'plan' : 'retail';  
        $v->aktif_sampai = $request->aktif_sampai;  
        $v->status = 0;  
        $v->save();

        $notify[] = ['success', 'New Voucher created successfully'];
        return back()->withNotify($notify);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function show(voucher $voucher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit(voucher $voucher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, voucher $voucher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(voucher $voucher)
    {
        //
    }
}
