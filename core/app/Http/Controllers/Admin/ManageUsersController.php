<?php
namespace App\Http\Controllers\Admin;

use App\Exports\ExportData;
use App\Exports\ExportUser;
use App\Exports\ExptUserGold;
use App\Exports\ExptUserQuery;
use App\Exports\ExptUserQueryPage;
use App\Exports\ExptUserView;
use App\Models\Deposit;
use App\Models\BvLog;
use App\Models\Gateway;
use App\Models\GeneralSetting;
use App\Http\Controllers\Controller;
use App\Models\bank;
use App\Models\bonus_reward;
use App\Models\Gold;
use App\Models\Plan;
use App\Models\rekening;
use App\Models\SupportTicket;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserLogin;
use App\Models\Survey;
use App\Models\ureward;
use App\Models\UserExtra;
use App\Models\WithdrawMethod;
use App\Models\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;
use Xendit\Xendit;

class ManageUsersController extends Controller
{
    public function allUsers()
    {
        $page_title = 'Manage Users';
        $empty_message = 'No user found';
        $users = User::select('users.*','t.created_at as join',db::raw("IF(email like '%+%' ,users.created_at,t.created_at) as j"))
        ->leftJoin('transactions as t', function($join){
            $join->on('t.user_id', '=', 'users.id')->where('t.remark','=','purchased_plan');
        })
        ->latest()->paginate(getPaginate());
        // $plan = '';
        $listplan = Plan::all();
        return view('admin.users.list', compact('page_title', 'empty_message', 'users','listplan'));
    }

    public function exportallUsers(Request $request){
        $dates = $request->date;
        // if (!$dates) {
        //     return back();
        // }
        $date = explode('-',$dates);
        if ($dates) {
            if(!(@strtotime($date[0]) && @strtotime($date[1]))){
                $notify[]=['error','Please provide valid date'];
                return back()->withNotify($notify);
            }
        }
        
        $start  = @$date[0];
        $end    = @$date[1];
        // $dateSearch = $dates;
        if (isset($request->search) && isset($request->plan_id) && isset($dates)) {
            # code...
            $q = User::query()->latest()->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email')
            ->where('username', 'like', "%$request->search%")
            ->orWhere('email', 'like', "%$request->search%")
            ->orWhere('no_bro', 'like', "%$request->search%")
            ->orWhere('mobile', 'like', "%$request->search%")
            ->orWhere('firstname', 'like', "%$request->search%")
            ->orWhere('lastname', 'like', "%$request->search%")
            ->where('plan_id',$request->plan_id)
            ->whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)]);
            return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');
        }else if (isset($request->search) && isset($request->plan_id) && !isset($dates)) {
            # code...
            $q = User::query()->latest()->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email')
            ->where('username', 'like', "%$request->search%")
            ->orWhere('email', 'like', "%$request->search%")
            ->orWhere('no_bro', 'like', "%$request->search%")
            ->orWhere('mobile', 'like', "%$request->search%")
            ->orWhere('firstname', 'like', "%$request->search%")
            ->orWhere('lastname', 'like', "%$request->search%")
            ->where('plan_id',$request->plan_id);
            return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');
        }else if (isset($request->search) && !isset($request->plan_id) && isset($dates)) {
            # code...
            $q = User::query()->latest()->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email')
            ->where('username', 'like', "%$request->search%")
            ->orWhere('email', 'like', "%$request->search%")
            ->orWhere('no_bro', 'like', "%$request->search%")
            ->orWhere('mobile', 'like', "%$request->search%")
            ->orWhere('firstname', 'like', "%$request->search%")
            ->orWhere('lastname', 'like', "%$request->search%")
            ->whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)]);
            return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');

        }elseif (!isset($request->search) && isset($request->plan_id) && isset($dates)) {
            $q = User::query()->latest()->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email')->where('plan_id',$request->plan_id)
            ->whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)]);
            return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');
            
        }elseif (!isset($request->search) && !isset($request->plan_id) && isset($dates)) {
            $q = User::query()->latest()->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email')
            ->whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)]);
            return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');

        }elseif (!isset($request->search) && isset($request->plan_id) && !isset($dates)) {
            $q = User::query()->latest()->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email')->where('plan_id',$request->plan_id);
            return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');

        }elseif (isset($request->search) && !isset($request->plan_id) && !isset($dates)) {
            return Excel::download(new ExptUserQuery($request->search), 'users.xlsx');
            # code...
        }else{
            if ($request->page != "Manage Users") {
                # code...
                // return Excel::download(new ExportUser, 'users.xlsx');
                // dd('s');
                if ($request->page == "Manage Active Users") {
                $q = User::query()->active()->latest()->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email');
                return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');
                }

                if ($request->page == "Banned Users") {
                $q = User::query()->banned()->latest()->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email');
                return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');
                }

                if ($request->page == "Verified Data Users") {
                $q = User::query()->where('is_kyc','2')->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email');
                return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');
                }

                if ($request->page == "Waiting For Verification Data Users") {
                $q = User::query()->where('is_kyc','1')->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email');
                return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');
                }

                if ($request->page == "Email Unverified Users") {
                $q = User::query()->emailUnverified()->latest()->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email');
                return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');
                }

                if ($request->page == "Email Verified Users") {
                $q = User::query()->emailVerified()->latest()->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email');
                return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');
                }

                if ($request->page == "Rejected Data Users") {
                $q = User::query()->where('is_kyc','3')->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email');
                return Excel::download(new ExptUserQueryPage($q), 'users.xlsx');
                }


            }else{
                return Excel::download(new ExportUser, 'users.xlsx');
            }

        }
        // dd($request->page);
    }

    public function activeUsers()
    {
        $page_title = 'Manage Active Users';
        $empty_message = 'No active user found';
        $users = User::active()->latest()->paginate(getPaginate());
        $listplan = Plan::all();
        return view('admin.users.list', compact('page_title', 'empty_message', 'users','listplan'));
    }

    public function bannedUsers()
    {
        $page_title = 'Banned Users';
        $empty_message = 'No banned user found';
        $users = User::banned()->latest()->paginate(getPaginate());
        $listplan = Plan::all();
        return view('admin.users.list', compact('page_title', 'empty_message', 'users','listplan'));
    }
    public function rejectDataUsers()
    {
        $page_title = 'Rejected Data Users';
        $empty_message = 'No rejected user found';
        $users = User::where('is_kyc','3')->paginate(getPaginate());
        $listplan = Plan::all();
        return view('admin.users.list', compact('page_title', 'empty_message', 'users','listplan'));
    }
    public function verifiedDataUsers()
    {
        $page_title = 'Verified Data Users';
        $empty_message = 'No Verified user found';
        $users = User::where('is_kyc','2')->paginate(getPaginate());
        $listplan = Plan::all();
        return view('admin.users.list', compact('page_title', 'empty_message', 'users','listplan'));
    }
    public function verificationDataUsers()
    {
        $page_title = 'Waiting For Verification Data Users';
        $empty_message = 'No Data user found';
        $users = User::where('is_kyc','1')->paginate(getPaginate());
        $listplan = Plan::all();
        return view('admin.users.list', compact('page_title', 'empty_message', 'users','listplan'));
    }

    public function emailUnverifiedUsers()
    {
        $page_title = 'Email Unverified Users';
        $empty_message = 'No email unverified user found';
        $users = User::emailUnverified()->latest()->paginate(getPaginate());
        $listplan = Plan::all();
        return view('admin.users.list', compact('page_title', 'empty_message', 'users','listplan'));
    }
    public function emailVerifiedUsers()
    {
        $page_title = 'Email Verified Users';
        $empty_message = 'No email verified user found';
        $users = User::emailVerified()->latest()->paginate(getPaginate());
        $listplan = Plan::all();
        return view('admin.users.list', compact('page_title', 'empty_message', 'users','listplan'));
    }

    public function userBalance(Request $request)
    {
        $search = $request->search;
        if (isset($search)) {
            # code...
            $page_title = 'Users Balance';
            $empty_message = 'No user found';
            $users = User::where('username', 'like',"%$search%")->where('status','!=',0)
                ->orWhere('no_bro', 'like',"%$search%")
                ->where('status','!=',0)
                ->orWhere('email', 'like',"%$search%")
                ->where('status','!=',0)->orderby('balance','DESC')->paginate(getPaginate());
            $listplan = Plan::all();
        }else{

            $page_title = 'Users Balance';
            $empty_message = 'No user found';
            $users = User::where('status','!=',0)->orderby('balance','DESC')->paginate(getPaginate());
            $listplan = Plan::all();
        }
        return view('admin.users.balance', compact('page_title', 'empty_message', 'users','listplan','search'));
    }

    public function exbalance(Request $request){
        $search = $request->search;
        if (isset($search)) {
            # code...
            $page_title = 'Users Balance';
            $empty_message = 'No user found';
            $users = User::query()->where('username', 'like',"%$search%")->where('status','!=',0)
                ->orWhere('no_bro', 'like',"%$search%")->where('status','!=',0)
                ->orWhere('email', 'like',"%$search%")->where('status','!=',0)->orderby('balance','DESC')->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email','balance');
            $listplan = Plan::all();
        }else{

            $page_title = 'Users Balance';
            $empty_message = 'No user found';
            $users = User::query()->where('status','!=',0)->orderby('balance','DESC')->select(db::raw("CONCAT(firstname, ' ',lastname ) AS nama"),'username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'no_bro','email','balance');
            $listplan = Plan::all();
        }
        return Excel::download(
            new ExportData($users), 'balance.xlsx');
    }


    // public function smsUnverifiedUsers()
    // {
    //     $page_title = 'SMS Unverified Users';
    //     $empty_message = 'No sms unverified user found';
    //     $users = User::smsUnverified()->latest()->paginate(getPaginate());
    //     return view('admin.users.list', compact('page_title', 'empty_message', 'users'));
    // }
    // public function smsVerifiedUsers()
    // {
    //     $page_title = 'SMS Verified Users';
    //     $empty_message = 'No sms verified user found';
    //     $users = User::smsVerified()->latest()->paginate(getPaginate());
    //     return view('admin.users.list', compact('page_title', 'empty_message', 'users'));
    // }



    public function search(Request $request, $scope)
    {
        $dates = $request->date;
        // if (!$dates) {
        //     return back();
        // }
        $date = explode('-',$dates);
        if ($dates) {
            if(!(@strtotime($date[0]) && @strtotime($date[1]))){
                $notify[]=['error','Please provide valid date'];
                return back()->withNotify($notify);
            }
        }
        
        $start  = @$date[0];
        $end    = @$date[1];
        $dateSearch = $dates;
        
        $search = $request->search;
        $plan = $request->plan_id;
        if(isset($search) && isset($plan) && isset($dates)){
            $users = User::where(function ($user) use ($search) {
                $user->where('username', 'like', "%$search%")
                    ->orWhere('email', 'like', "%$search%")
                    ->orWhere('no_bro', 'like', "%$search%")
                    ->orWhere('mobile', 'like', "%$search%")
                    ->orWhere('firstname', 'like', "%$search%")
                    ->orWhere('lastname', 'like', "%$search%");
            });
            $users = $users->where('plan_id', $plan);
            $users = $users->whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)]);
        }elseif (isset($search) && !isset($plan) && isset($dates)) {
            $users = User::where(function ($user) use ($search) {
                $user->where('username', 'like', "%$search%")
                    ->orWhere('email', 'like', "%$search%")
                    ->orWhere('no_bro', 'like', "%$search%")
                    ->orWhere('mobile', 'like', "%$search%")
                    ->orWhere('firstname', 'like', "%$search%")
                    ->orWhere('lastname', 'like', "%$search%");
            });
            $users = $users->whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)]);
        }elseif (isset($search) && !isset($plan) && !isset($dates)) {
            $users = User::where(function ($user) use ($search) {
                $user->where('username', 'like', "%$search%")
                    ->orWhere('email', 'like', "%$search%")
                    ->orWhere('no_bro', 'like', "%$search%")
                    ->orWhere('mobile', 'like', "%$search%")
                    ->orWhere('firstname', 'like', "%$search%")
                    ->orWhere('lastname', 'like', "%$search%");
            });
        }elseif (isset($search) && isset($plan) && !isset($dates)) {
            $users = User::where(function ($user) use ($search) {
                $user->where('username', 'like', "%$search%")
                    ->orWhere('email', 'like', "%$search%")
                    ->orWhere('no_bro', 'like', "%$search%")
                    ->orWhere('mobile', 'like', "%$search%")
                    ->orWhere('firstname', 'like', "%$search%")
                    ->orWhere('lastname', 'like', "%$search%");
            });
            $users = $users->where('plan_id', $plan);
        }
        elseif (!isset($search) && isset($plan)  && isset($dates)) {
            $users = user::where('plan_id',$plan);
            $users = $users->whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)]);
        }elseif (!isset($search) && isset($plan)  && !isset($dates)) {
            $users = user::where('plan_id',$plan);
        }elseif (!isset($search) && !isset($plan)  && isset($dates)) {
            $users = user::whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)]);
        }else{
            $users = user::where('id','!=',0);
        }

        $page_title = '';
        switch ($scope) {
            case 'active':
                $page_title .= 'Active ';
                $users = $users->where('status', 1);
                break;
            case 'banned':
                $page_title .= 'Banned';
                $users = $users->where('status', 0);
                break;
            case 'emailUnverified':
                $page_title .= 'Email Unerified ';
                $users = $users->where('ev', 0);
                break;
            case 'smsUnverified':
                $page_title .= 'SMS Unverified ';
                $users = $users->where('sv', 0);
                break;
        }
        $users = $users->latest()->paginate(getPaginate());
        $page_title .= 'User Search - ' . $search;
        $empty_message = 'No search result found';
        $listplan = Plan::all();
        return view('admin.users.list', compact('page_title', 'search', 'scope', 'empty_message', 'users','plan','dateSearch','listplan'));
    }


    public function detail($id)
    {
        $page_title         = 'User Detail';
        $user               = User::where('id', $id)->with('userExtra')->first();
        $ref_id             = User::find($user->ref_id);
        // $bank = bank::all();
        $totalDeposit       = Deposit::where('user_id',$user->id)->where('status',1)->sum('amount');
        $totalWithdraw      = Withdrawal::where('user_id',$user->id)->where('status',1)->sum('amount');
        $totalTransaction   = Transaction::where('user_id',$user->id)->count();

        $totalBvCut         = BvLog::where('user_id',$user->id)->where('trx_type', '-')->sum('amount');
        
        $emas               = Gold::where('user_id',$user->id)->where('golds.status','=','0')->join('products','products.id','=','golds.prod_id')->select('golds.*',db::raw('COALESCE(SUM(products.price * golds.qty),0) as total_rp'),db::raw('COALESCE(sum(products.weight * golds.qty ),0) as total_wg'))->groupBy('golds.user_id')->first();
        
        // $va           = env('IPAY_VA'); //get on iPaymu dashboard
        // $secret       = env('IPAY_SECRET'); //get on iPaymu dashboard

        // if (env('APP_ENV') != 'production') {
        //     # code...
        //     $url          = 'https://sandbox.ipaymu.com/api/v2/banklist'; // for development mode
        // }else{
        //     $url          = 'https://my.ipaymu.com/api/v2/banklist'; // for production mode
        // }
        
        // $method       = 'POST';

        // $body = '';

        //         $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
        //         $requestBody  = strtolower(hash('sha256', $jsonBody));
        //         $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
        //         $signature    = hash_hmac('sha256', $stringToSign, $secret);
        //         $timestamp    = Date('YmdHis');

        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        // CURLOPT_URL => $url,
        // CURLOPT_RETURNTRANSFER => true,
        // CURLOPT_ENCODING => '',
        // CURLOPT_MAXREDIRS => 10,
        // CURLOPT_TIMEOUT => 0,
        // CURLOPT_FOLLOWLOCATION => true,
        // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        // CURLOPT_CUSTOMREQUEST => 'POST',
        // CURLOPT_HTTPHEADER => array(
        //     'Content-Type: application/json',
        //     'signature: '.$signature,
        //     'va: '.$va,
        //     'timestamp: ' .$timestamp
        // ),
        // ));

        // $response = curl_exec($curl);

        // curl_close($curl);
        // $res = json_decode($response);
        // // echo $response;
        // // dd($res->Data->bank);
        // $bank = $res->Data->bank;
        // adminlog(Auth::guard('admin')->user()->id,'See User Detail '.$user->username);

        Xendit::setApiKey(config('xendit.key_auth'));
        
        $getDisbursementsBanks = \Xendit\Disbursements::getAvailableBanks();
        $bank = $getDisbursementsBanks;
        return view('admin.users.detail', compact('page_title','ref_id','user','totalDeposit',
            'totalWithdraw','totalTransaction',  'totalBvCut','emas','bank'));
    }

    public function goldDetail($id){
        $user = user::where('id',$id)->first();
        $page_title         = 'Gold Invest Detail : '.$user->username;
        $empty_message = 'Gold Invest Not found.';
        $gold  = Gold::where('user_id',$id)->where('golds.qty','!=',0)->where('golds.status','=','0')->join('products','products.id','=','golds.prod_id')->select('products.*','golds.qty',db::raw('SUM(products.price * golds.qty) as total_rp'),db::raw('sum(products.weight * golds.qty ) as total_wg'))->groupBy('golds.prod_id')
        ->paginate(getPaginate());

        // adminlog(Auth::guard('admin')->user()->id,'See User Gold Detail '.$user->username);
        return view('admin.users.gold',compact('page_title', 'empty_message','gold'));
        // return view('admin.users.gold',compact('page_title','emas'))
    }


    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $request->validate([
            'firstname' => 'required|max:60',
            'lastname' => 'required|max:60',
            'email' => 'required|email|max:160|unique:users,email,' . $user->id,
        ]);

        if ($request->email != $user->email && User::whereEmail($request->email)->whereId('!=', $user->id)->count() > 0) {
            $notify[] = ['error', 'Email already exists.'];
            return back()->withNotify($notify);
        }
        if ($request->mobile != $user->mobile && User::where('mobile', $request->mobile)->whereId('!=', $user->id)->count() > 0) {
            $notify[] = ['error', 'Phone number already exists.'];
            return back()->withNotify($notify);
        }

        $user->mobile = $request->mobile;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->address = [
                            'address' => $request->address,
                            'city' => $request->city,
                            'state' => $request->state,
                            'zip' => $request->zip,
                            'country' => $request->country,
                        ];
        $user->status = $request->status ? 1 : 0;
        $user->ev = $request->ev ? 1 : 0;
        $user->sv = $request->sv ? 1 : 0;
        $user->ts = $request->ts ? 1 : 0;
        $user->tv = $request->tv ? 1 : 0;
        $user->save();

        adminlog(Auth::guard('admin')->user()->id,'Update User Detail '.$user->username);

        $notify[] = ['success', 'User detail has been updated'];
        return redirect()->back()->withNotify($notify);
    }

    public function rek(Request $request, $id)
    {
        $this->validate($request, [
            'bank_name' => 'required',
            'acc_name' => 'required',
            'acc_number' => 'required'
        ]);

        $rek = rekening::where('user_id',$id)->first();
        
        if ($rek) {
            # code...
            $reks = rekening::where('user_id',$id)->first();
        }else{
            $reks = new rekening();
            $reks->user_id = $id; 
        }
        $reks->nama_bank = $request->bank_name;
        $reks->nama_akun = $request->acc_name;
        $reks->no_rek = $request->acc_number;
        $reks->kota_cabang = $request->kota_cabang;
        $reks->save();

        $user = User::where('id','=',$id)->first();

        adminlog(Auth::guard('admin')->user()->id,'Update User Bank Account Detail '.$user->username);

        $notify[] = ['success', 'User Bank Account detail has been updated'];
        return redirect()->back()->withNotify($notify);
    }

    public function addSubBalance(Request $request, $id)
    {
        // dd($request->all());
        $request->validate(['amount' => 'required|numeric|gt:0']);

        $user = User::findOrFail($id);
        $amount = getAmount($request->amount);
        $general = GeneralSetting::first(['cur_text','cur_sym']);
        $trx = getTrx();

        DB::beginTransaction();

        try {
        if ($request->act) {
            $user->balance += $amount;
            $user->save();
            $notify[] = ['success', $general->cur_sym . $amount . ' has been added to ' . $user->username . ' balance'];


            $transaction = new Transaction();
            $transaction->user_id = $user->id;
            $transaction->amount = $amount;
            $transaction->post_balance = getAmount($user->balance);
            $transaction->charge = 0;
            $transaction->trx_type = '+';
            $transaction->details = !isset($request->note) ? 'Added Balance Via Admin' : $request->note;
            $transaction->trx =  $trx;
            $transaction->save();


            notify($user, 'BAL_ADD', [
                'trx' => $trx,
                'amount' => $amount,
                'currency' => $general->cur_text,
                'post_balance' => getAmount($user->balance),
            ]);

            adminlog(Auth::guard('admin')->user()->id,'Add Balance To User '.$user->username);

        } else {
            if ($amount > $user->balance) {
                $notify[] = ['error', $user->username . ' has insufficient balance.'];
                return back()->withNotify($notify);
            }
            $user->balance -= $amount;
            $user->save();



            $transaction = new Transaction();
            $transaction->user_id = $user->id;
            $transaction->amount = $amount;
            $transaction->post_balance = getAmount($user->balance);
            $transaction->charge = 0;
            $transaction->trx_type = '-';
            $transaction->details = !isset($request->note) ? 'Subtract Balance Via Admin' : $request->note;
            $transaction->trx =  $trx;
            $transaction->save();


            notify($user, 'BAL_SUB', [
                'trx' => $trx,
                'amount' => $amount,
                'currency' => $general->cur_text,
                'post_balance' => getAmount($user->balance)
            ]);

            adminlog(Auth::guard('admin')->user()->id,'Subtract Balance To User '.$user->username);
            $notify[] = ['success', $general->cur_sym . $amount . ' has been subtracted from ' . $user->username . ' balance'];
        }
        DB::commit();
        return back()->withNotify($notify);

        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            $notify[] = ['error', 'Action failed, please try again.'.$e];
            return back()->withNotify($notify);
        }
    }

    public function userLoginHistory($id)
    {
        $user = User::findOrFail($id);
        $page_title = 'User Login History - ' . $user->username;
        $empty_message = 'No users login found.';
        $login_logs = $user->login_logs()->latest()->paginate(getPaginate());
        return view('admin.users.logins', compact('page_title', 'empty_message', 'login_logs'));
    }

    public function userRef($id)
    {

        $empty_message = 'No user found';
        $user = User::findOrFail($id);
        $page_title = 'Referred By ' . $user->username;
        $listplan = Plan::all();
        $users = User::where('ref_id', $id)->latest()->paginate(getPaginate());
        return view('admin.users.list', compact('page_title', 'empty_message', 'users','listplan'));
    }

    public function showEmailSingleForm($id)
    {
        $user = User::findOrFail($id);
        $page_title = 'Send Email To: ' . $user->username;
        return view('admin.users.email_single', compact('page_title', 'user'));
    }

    public function sendEmailSingle(Request $request, $id)
    {
        $request->validate([
            'message' => 'required|string|max:65000',
            'subject' => 'required|string|max:190',
        ]);

        $user = User::findOrFail($id);
        sendGeneralEmail($user->email, $request->subject, $request->message, $user->username);
        adminlog(Auth::guard('admin')->user()->id,'Send Email To User '.$user->username);
        $notify[] = ['success', $user->username . ' will receive an email shortly.'];
        return back()->withNotify($notify);
    }

    public function transactions(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if ($request->search) {
            $search = $request->search;
            $page_title = 'Search User Transactions : ' . $user->username;
            $transactions = $user->transactions()->where('trx', $search)->with('user')->latest()->paginate(getPaginate());
            $empty_message = 'No transactions';
            return view('admin.reports.transactions', compact('page_title', 'search', 'user', 'transactions', 'empty_message'));
        }
        $page_title = 'User Transactions : ' . $user->username;
        $transactions = $user->transactions()->with('user')->latest()->paginate(getPaginate());
        $empty_message = 'No transactions';
        return view('admin.reports.transactions', compact('page_title', 'user', 'transactions', 'empty_message'));
    }

    public function deposits(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $userId = $user->id;
        if ($request->search) {
            $search = $request->search;
            $page_title = 'Search User Deposits : ' . $user->username;
            $deposits = $user->deposits()->where('trx', $search)->latest()->paginate(getPaginate());
            $empty_message = 'No deposits';
            return view('admin.deposit.log', compact('page_title', 'search', 'user', 'deposits', 'empty_message','userId'));
        }

        $page_title = 'User Deposit : ' . $user->username;
        $deposits = $user->deposits()->latest()->paginate(getPaginate());
        $empty_message = 'No deposits';
        $scope = 'all';
        return view('admin.deposit.log', compact('page_title', 'user', 'deposits', 'empty_message','userId','scope'));
    }


    public function depViaMethod($method,$type = null,$userId){
        $method = Gateway::where('alias',$method)->firstOrFail();
        $user = User::findOrFail($userId);
        if ($type == 'approved') {
            $page_title = 'Approved Payment Via '.$method->name;
            $deposits = Deposit::where('method_code','>=',1000)->where('user_id',$user->id)->where('method_code',$method->code)->where('status', 1)->latest()->with(['user', 'gateway'])->paginate(getPaginate());
        }elseif($type == 'rejected'){
            $page_title = 'Rejected Payment Via '.$method->name;
            $deposits = Deposit::where('method_code','>=',1000)->where('user_id',$user->id)->where('method_code',$method->code)->where('status', 3)->latest()->with(['user', 'gateway'])->paginate(getPaginate());
        }elseif($type == 'successful'){
            $page_title = 'Successful Payment Via '.$method->name;
            $deposits = Deposit::where('status', 1)->where('user_id',$user->id)->where('method_code',$method->code)->latest()->with(['user', 'gateway'])->paginate(getPaginate());
        }elseif($type == 'pending'){
            $page_title = 'Pending Payment Via '.$method->name;
            $deposits = Deposit::where('method_code','>=',1000)->where('user_id',$user->id)->where('method_code',$method->code)->where('status', 2)->latest()->with(['user', 'gateway'])->paginate(getPaginate());
        }else{
            $page_title = 'Payment Via '.$method->name;
            $deposits = Deposit::where('status','!=',0)->where('user_id',$user->id)->where('method_code',$method->code)->latest()->with(['user', 'gateway'])->paginate(getPaginate());
        }
        $page_title = 'Deposit History: '.$user->username.' Via '.$method->name;
        $methodAlias = $method->alias;
        $empty_message = 'Deposit Log';
        return view('admin.deposit.log', compact('page_title', 'empty_message', 'deposits','methodAlias','userId'));
    }



    public function withdrawals(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if ($request->search) {
            $search = $request->search;
            $page_title = 'Search User Withdrawals : ' . $user->username;
            $withdrawals = $user->withdrawals()->where('trx', 'like',"%$search%")->latest()->paginate(getPaginate());
            $empty_message = 'No withdrawals';
            return view('admin.withdraw.withdrawals', compact('page_title', 'user', 'search', 'withdrawals', 'empty_message'));
        }
        $page_title = 'User Withdrawals : ' . $user->username;
        $withdrawals = $user->withdrawals()->latest()->paginate(getPaginate());
        $empty_message = 'No withdrawals';
        $userId = $user->id;
        return view('admin.withdraw.withdrawals', compact('page_title', 'user', 'withdrawals', 'empty_message','userId'));
    }

    public  function withdrawalsViaMethod($method,$type,$userId){
        $method = WithdrawMethod::findOrFail($method);
        $user = User::findOrFail($userId);
        if ($type == 'approved') {
            $page_title = 'Approved Withdrawal of '.$user->username.' Via '.$method->name;
            $withdrawals = Withdrawal::where('status', 1)->where('user_id',$user->id)->with(['user','method'])->latest()->paginate(getPaginate());
        }elseif($type == 'rejected'){
            $page_title = 'Rejected Withdrawals of '.$user->username.' Via '.$method->name;
            $withdrawals = Withdrawal::where('status', 3)->where('user_id',$user->id)->with(['user','method'])->latest()->paginate(getPaginate());

        }elseif($type == 'pending'){
            $page_title = 'Pending Withdrawals of '.$user->username.' Via '.$method->name;
            $withdrawals = Withdrawal::where('status', 2)->where('user_id',$user->id)->with(['user','method'])->latest()->paginate(getPaginate());
        }else{
            $page_title = 'Withdrawals of '.$user->username.' Via '.$method->name;
            $withdrawals = Withdrawal::where('status', '!=', 0)->where('user_id',$user->id)->with(['user','method'])->latest()->paginate(getPaginate());
        }
        $empty_message = 'Withdraw Log Not Found';
        return view('admin.withdraw.withdrawals', compact('page_title', 'withdrawals', 'empty_message','method'));
    }

    public function showEmailAllForm()
    {
        $page_title = 'Send Email To All Users';
        return view('admin.users.email_all', compact('page_title'));
    }

    public function sendEmailAll(Request $request)
    {
        $request->validate([
            'message' => 'required|string|max:65000',
            'subject' => 'required|string|max:190',
        ]);

        foreach (User::where('status', 1)->cursor() as $user) {
            sendGeneralEmail($user->email, $request->subject, $request->message, $user->username);
        }

        adminlog(Auth::guard('admin')->user()->id,'Send Email To All User ');

        $notify[] = ['success', 'All users will receive an email shortly.'];
        return back()->withNotify($notify);
    }

    public function tree($username){

        $user = User::where('username',$username)->first();

        if($user){
            $data['tree'] = showTreePage($user->id);
            $data['page_title'] = "Tree of ".$user->fullname;
            return view( 'admin.users.tree', $data);
        }

        $notify[] = ['error', 'Tree Not Found!!'];
        return redirect()->route('admin.dashboard')->withNotify($notify);

    }

    public function otherTree(Request $request, $username = null)
    {
        if ($request->username) {
            $user = User::where('username', $request->username)->first();
        } else {
            $user = User::where('username', $username)->first();
        }
        if ($user) {
            $data['tree'] = showTreePage($user->id);
            $data['page_title'] = "Tree of " . $user->fullname;
            return view( 'admin.users.tree', $data);
        }

        $notify[] = ['error', 'Tree Not Found!!'];
        return redirect()->route('admin.dashboard')->withNotify($notify);
    }

    public function survey($id){
        $user = User::findOrFail($id);
        $page_title = 'User Completed Survey : '.$user->username;
        $empty_message = 'No completed survey found';
        $all_survey = Survey::whereJsonContains('users', $user->id)->orderBy('last_report', 'DESC')->paginate(getPaginate());
        return view('admin.survey.report', compact('page_title','all_survey','empty_message', 'id'));
    }

    public function login($id){
        $user = User::findOrFail($id);
        adminlog(Auth::guard('admin')->user()->id,'Login To User '.$user->username);
        Auth::login($user);
        return redirect()->route('user.home');
    }

    public function verify($id){
        $user = User::findOrFail($id);
        $user->is_kyc = 2;
        $user->save();

        adminlog(Auth::guard('admin')->user()->id,'Approve KYC User '.$user->username);
        $notify[] = ['success', 'User successfully verify!!'];
        return back()->withNotify($notify);
    }
    public function reject(Request $request,$id){
        $user = User::findOrFail($id);
        $user->is_kyc = 3;
        $user->kyc_note = $request->note;
        $user->save();

        adminlog(Auth::guard('admin')->user()->id,'Reject KYC User '.$user->username);
        $notify[] = ['success', 'User successfully rejected!!'];
        return back()->withNotify($notify);
    }

    public function setUserPlacement($id,Request $request){
        // dd($request->all());
        $user = user::where('id',$id)->first();
        $userextra = UserExtra::where('user_id',$id)->first();
        $usparent = User::where('id',$user->pos_id)->first();
        $usparentextra = UserExtra::where('user_id',$usparent->id)->first();
        $parent = user::where('no_bro',$request->no_bro)->first();
        //bro tidak ditemukan
        if (!$parent) {
            $notify[] = ['error', 'BRO number not found'];
            return response()->json($notify);
        }

        if ($userextra->left >= 30 || $userextra->right >= 30) {
            # code...
            $notify[] = ['error', 'Cannot change placement because user already have more than 30 leg'];
            return response()->json($notify);
        }
        
        $parentextra = UserExtra::where('user_id',$parent->id)->first();

        $lparent = user::where('pos_id',$parent->id)->where('position',1)->first();
        $rparent = user::where('pos_id',$parent->id)->where('position',2)->first();

        //slot penuh
        if ($lparent && $rparent) {
            # code...
            $notify[] = ['error', 'BRO number has not empty slot'];
            return response()->json($notify);
        }

        //kiri penuh
        if ($lparent && $request->position == 1) {
            # code...
            $notify[] = ['error', 'Left slot BRO number has not empty'];
            return response()->json($notify);
        }

        //kanan penuh
        if ($rparent && $request->position == 2) {
            # code...
            $notify[] = ['error', 'Right slot BRO number has not empty'];
            return response()->json($notify);
        }
        // $count = ($userextra->left + $userextra->right) + 1;

        // if ($request->no_bro == $usparent->no_bro) {
        //     # code...
        //     // dd('s');
        //     if ($request->position == 1) {
        //         # code...
        //         $usparentextra->paid_left += ($userextra->paid_left + $userextra->paid_right) + 1;
        //         $usparentextra->left += ($userextra->left + $userextra->right) + 1;
        //         $usparentextra->paid_right -= ($userextra->paid_left + $userextra->paid_right) + 1;
        //         $usparentextra->right -= ($userextra->left + $userextra->right) + 1;
        //         $usparentextra->save();
        //     }

        //     if ($request->position == 2) {
        //         # code...
        //         $usparentextra->paid_left -= ($userextra->paid_left + $userextra->paid_right) + 1;
        //         $usparentextra->left -= ($userextra->left + $userextra->right) + 1;
        //         $usparentextra->paid_right += ($userextra->paid_left + $userextra->paid_right) + 1;
        //         $usparentextra->right += ($userextra->left + $userextra->right) + 1;
        //         $usparentextra->save();
        //     }
        // }





        // if ($request->position == 1) {
        //     # code...

        //     $parentextra->paid_left += ($userextra->paid_left + $userextra->paid_right) + 1;
        //     $parentextra->left += ($userextra->left + $userextra->right) + 1;
        //     $parentextra->save();

        // }

        // if ($request->position == 2) {
        //     # code...
        //     $parentextra->paid_right += ($userextra->paid_left + $userextra->paid_right) + 1;
        //     $parentextra->right += ($userextra->left + $userextra->right) + 1;
        //     $parentextra->save();

        // }



        // if ($user->position == 1) {
        //     # code...
        //     $usparentextra->paid_left -= ($userextra->paid_left + $userextra->paid_right) + 1;
        //     $usparentextra->left -= ($userextra->left + $userextra->right) + 1;
        //     $usparentextra->save();

        // }

        // if ($user->position == 2) {
        //     # code...
        //     $usparentextra->paid_right -= ($userextra->paid_left + $userextra->paid_right) + 1;
        //     $usparentextra->right -= ($userextra->left + $userextra->right) + 1;
        //     $usparentextra->save();

        // }



        $user->pos_id = $parent->id;
        $user->position = $request->position;
        $user->save();
        updatePaidCount3($user->id,$usparent->id,$parent->id);
        adminlog(Auth::guard('admin')->user()->id,'Update Placement '.$user->username);

        $notify[] = ['success', 'The user has successfully changed his placement'];
        return response()->json($notify);
    }

    public function updateCounting($id,Request $request){
        $userextra = UserExtra::where('user_id',$id)->first();
        $userextra->paid_left = $request->left;
        $userextra->left = $request->left;
        $userextra->paid_right = $request->right;
        $userextra->right = $request->right;
        $userextra->save();

        $user = user::where('id',$id)->first();
        
        adminlog(Auth::guard('admin')->user()->id,'Update Counting '.$user->username);

        $notify[] = ['success', 'Update Counting Successfully'];
        return response()->json($notify);
    }

    public function userGold(Request $request){
        if ($request->search) {
            $search = $request->search;
            $page_title = 'User Golds';
            $empty_message = 'No user found';
            $users = User::join('golds','golds.user_id','=','users.id')->join('products','products.id','=','golds.prod_id')
            ->where('users.email', 'like',"%$search%")
            ->Orwhere('users.username', 'like',"%$search%")
            ->select('users.id as id','users.username as username', 'users.firstname as fr','users.lastname as ls', 'users.email as email', db::raw('sum(products.weight * golds.qty) as emas'))
            ->groupby('users.id')
            ->paginate(getPaginate());

            // dd($users);
            return view('admin.users.gd', compact('page_title','search', 'empty_message', 'users'));
        }
        $page_title = 'User Golds';
        $empty_message = 'No user found';
        $users = User::join('golds','golds.user_id','=','users.id')->join('products','products.id','=','golds.prod_id')
        ->select('users.id as id','users.username as username', 'users.firstname as fr','users.lastname as ls', 'users.email as email', db::raw('sum(products.weight * golds.qty) as emas'))
        ->groupby('users.id')
        ->paginate(getPaginate());

        // dd($users);
        return view('admin.users.gd', compact('page_title', 'empty_message', 'users'));
    }

    public function exportUserGold(Request $request){
        if ($request->search) {
            # code...
            $search = $request->search;
            return Excel::download(new ExptUserGold(User::query()->join('golds','golds.user_id','=','users.id')->join('products','products.id','=','golds.prod_id')
            ->where('users.email', 'like',"%$search%")
            ->Orwhere('users.username', 'like',"%$search%")
            ->select('users.id as id','users.username as username', db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"), 'users.email as email', db::raw('sum(products.weight * golds.qty) as emas'))
            ->groupby('users.id')), 'users_golds.xlsx');
        }else{
            return Excel::download(new ExptUserGold(User::query()->join('golds','golds.user_id','=','users.id')->join('products','products.id','=','golds.prod_id')
            ->select('users.username as username', db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"), 'users.email as email', db::raw('sum(products.weight * golds.qty) as emas'))
            ->groupby('users.id')), 'users_golds.xlsx');
        }
    }

    public function updateNik($id, Request $request){
        // dd($request->all());
        $user = User::where('id',$id)->first();
        $user->nik = $request->nik;
        $user->save();

        adminlog(Auth::guard('admin')->user()->id,'Update Data KYC User '.$user->username);

        $notify[] = ['success', 'User Data Verification has been updated'];
        return redirect()->back()->withNotify($notify);
    }

    public function topLeader(Request $request){
        $search = '';
        $type = 0;
        if (isset($request->search)) {
            $users = User::join('user_extras as ue','ue.user_id','=','users.id')
            ->where('plan_id','=',1)
            ->where('users.email','like','%'.$request->search.'%')
            ->whereNotIn('users.id',[153,154,155,156,157,158,158,178,179,181,182,183,184,185,186,327])
            ->orwhere('users.username','like','%'.$request->search.'%') 
            ->where('plan_id','=',1)
            ->whereNotIn('users.id',[153,154,155,156,157,158,158,178,179,181,182,183,184,185,186,327])
            ->orwhere('users.mobile','like','%'.$request->search.'%')
            ->where('plan_id','=',1)
            ->whereNotIn('users.id',[153,154,155,156,157,158,158,178,179,181,182,183,184,185,186,327])
            ->select('users.*',db::raw("(ue.left + ue.right) as bro"), 'ue.left as bro_left' , 'ue.right as bro_right')
            ->orderby('bro','DESC')
            ->limit(10)->paginate(getPaginate());
            $page_title = 'Leaderboard - Search '.$search;
            $search = $request->search;
        }

        if (isset($request->type)) {
            # code...
            $type = $request->type;
            if ($type == 0) {
                # code...
                $users = User::where('plan_id','=',1)
                ->join('user_extras as ue','ue.user_id','=','users.id')
                ->whereNotIn('users.id',[153,154,155,156,157,158,158,178,179,181,182,183,184,185,186,327])
                // ->where('users.email','not like','%+%')
                ->select('users.*',db::raw("(ue.left + ue.right) as bro"), 'ue.left as bro_left' , 'ue.right as bro_right')
                ->orderby('bro','DESC')
                ->limit(10)->paginate(getPaginate());
                $page_title = 'Leaderboard - By Total';
            }
            if ($type == 1) {
                # code...
                $users = User::where('plan_id','=',1)
                ->join('user_extras as ue','ue.user_id','=','users.id')
                ->whereNotIn('users.id',[153,154,155,156,157,158,158,178,179,181,182,183,184,185,186,327])
                ->where('ue.left','>=',51)
                ->where('ue.right','>=',51)
                ->select('users.*',db::raw("(ue.left + ue.right) as bro"), 'ue.left as bro_left' , 'ue.right as bro_right')
                ->orderby('bro','DESC')
                ->limit(10)->paginate(getPaginate());
                $page_title = 'Leaderboard - Leader';
            }
            if ($type == 2) {
                # code...
                $users = User::where('plan_id','=',1)
                ->join('user_extras as ue','ue.user_id','=','users.id')
                ->whereNotIn('users.id',[153,154,155,156,157,158,158,178,179,181,182,183,184,185,186,327])
                ->where('ue.left','>=',750)
                ->where('ue.right','>=',750)
                ->select('users.*',db::raw("(ue.left + ue.right) as bro"), 'ue.left as bro_left' , 'ue.right as bro_right')
                ->orderby('bro','DESC')
                ->limit(10)->paginate(getPaginate());
                $page_title = 'Leaderboard - Marshal';
            }
            if ($type == 3) {
                # code...
                $users = User::where('plan_id','=',1)
                ->join('user_extras as ue','ue.user_id','=','users.id')
                ->whereNotIn('users.id',[153,154,155,156,157,158,158,178,179,181,182,183,184,185,186,327])
                ->where('ue.left','>=',1500)
                ->where('ue.right','>=',1500)
                ->select('users.*',db::raw("(ue.left + ue.right) as bro"), 'ue.left as bro_left' , 'ue.right as bro_right')
                ->orderby('bro','DESC')
                ->limit(10)->paginate(getPaginate());
                $page_title = 'Leaderboard - Centurion';
            }
        }

        if (!isset($request->search) && !isset($request->type)) {
            $users = User::where('plan_id','=',1)
            ->join('user_extras as ue','ue.user_id','=','users.id')
            ->whereNotIn('users.id',[153,154,155,156,157,158,158,178,179,181,182,183,184,185,186,327])
            // ->where('users.email','not like','%+%')
            ->select('users.*',db::raw("(ue.left + ue.right) as bro"), 'ue.left as bro_left' , 'ue.right as bro_right')
            ->orderby('bro','DESC')
            ->limit(10)->paginate(getPaginate());
            $page_title = 'Leaderboard - By Total';
        }

        
        $empty_message = 'No user found';


        // dd($user);
        return view('admin.users.top', compact('page_title', 'empty_message', 'users','search','type'));
    }

    public function addBROqty($id, Request $request){
        // dd($request->all());
        $this->validate($request, [
            'qty' => 'required|integer|min:1',
        ]);
        $user = User::find($id);
        $plan = Plan::where('id', $user->plan_id)->where('status', 1)->firstOrFail();
        $gnl = GeneralSetting::first();
        if ($user->balance < ($plan->price * $request->qty)) {
            $notify[] = ['error', 'Insufficient Balance'];
            return back()->withNotify($notify);
        }
        $user->balance -= ($plan->price * $request->qty);
        $user->total_invest += ($plan->price * $request->qty);
        $user->bro_qty += $request->qty;
        $user->save();

        $trx = $user->transactions()->create([
            'amount' => $plan->price * $request->qty,
            'trx_type' => '-',
            'details' => 'Added SP quantity for '.$request->qty.' SP',
            'remark' => 'purchased_plan',
            'trx' => getTrx(),
            'post_balance' => getAmount($user->balance),
        ]);

        adminlog(Auth::guard('admin')->user()->id,'Added SP quantity for '.$request->qty.' SP to user '.$user->username);

        $notify[] = ['success', 'Added SP quantity for '.$request->qty.' BRO Successfully'];
        return back()->withNotify($notify);
    }

    public function userRewardList(Request $request){
        if (isset($request->search) && isset($request->reward_id)) {
            # code...
            $search = $request->search;
            $reward = $request->reward_id;
            $users = ureward::where('reward_id',$request->reward_id)->where(function ($q) use ($search) {
                $q->WhereHas('user', function ($user) use ($search) {
                        $user->where('username', 'like',"%$search%");
                        $user->orwhere('email', 'like',"%$search%");
                        $user->orwhere('firstname', 'like',"%$search%");
                        $user->orwhere('lastname', 'like',"%$search%");
                    });
            });
            $users = $users->paginate(getPaginate());
        }elseif(!isset($request->search) && isset($request->reward_id)){
            $reward = $request->reward_id;
            $search = '';
            $users = ureward::where('reward_id',$request->reward_id)->paginate(getPaginate());
        }elseif(isset($request->search) && !isset($request->reward_id)){
            $search = $request->search;
            $reward = '';
            $users = ureward::where(function ($q) use ($search) {
                $q->WhereHas('user', function ($user) use ($search) {
                        $user->where('username', 'like',"%$search%");
                        $user->orwhere('email', 'like',"%$search%");
                        $user->orwhere('firstname', 'like',"%$search%");
                        $user->orwhere('lastname', 'like',"%$search%");
                    });
            });
            $users = $users->paginate(getPaginate());
        }
        else{
            $users = ureward::paginate(getPaginate());
            $search = '';
            $reward = '';
        }
        
        
        $page_title = 'User Reward';
        $empty_message = 'No user found';
        $listreward = bonus_reward::all(); 
        // dd($listreward);
        return view('admin.users.reward', compact('page_title', 'empty_message', 'users','search','listreward','reward'));
    }
    public function userRewardScan(){
        $page_title = 'Scan User Reward';
        $empty_message = 'No user found';
        return view('admin.users.scan-reward', compact('page_title', 'empty_message'));
    }


    public function snipperLeft(Request $request){
        // dd($request->all());
        // Daftar nama depan
        $nama_depan = array(
            "Adi", "Bayu", "Cinta", "Dian", "Edi", "Farida", "Gatot", "Hasan", "Irma", "Jaya",
            "Kusuma", "Lina", "Mira", "Nita", "Oktavia", "Putri", "Rika", "Sari", "Tri", "Umi",
            "Vera", "Wahyu", "Yanto", "Zainab", "Bagus", "Chandra", "Dewi", "Erwin", "Fandi",
            "Gita", "Hadi", "Intan", "Joko", "Kartika", "Lutfi", "Mega", "Nando", "Oscar",
            "Puspita", "Rizky", "Sita", "Tomi", "Umi", "Vino", "Widi", "Yuli", "Zaki"
        );

        // Daftar nama belakang
        $nama_belakang = array(
            "Anwar", "Basuki", "Cahyadi", "Dewi", "Efendi", "Fitriani", "Gunawan", "Hidayat",
            "Irawan", "Junaedi", "Kurnia", "Lubis", "Maulana", "Nasution", "Octaviani",
            "Puspitasari", "Rachmawati", "Suryadi", "Tari", "Utami", "Vania", "Wibisono",
            "Yulianti", "Zulfikar", "Aulia", "Bintang", "Cahya", "Dharma", "Eka", "Febri",
            "Gani", "Hariyanto", "Indra", "Juwita", "Kurniawan", "Larasati", "Mulyadi", "Nanda",
            "Octavia", "Prabowo", "Ratih", "Sasmita", "Teguh", "Umar", "Viona", "Wita", "Yuni",
            "Zahra"
        );

        // Daftar kata sambung
        $kata_sambung = array(
            "", "", "", "", "", "", "", "", "", "", "bin", "binti", "putra", "putri"
        );

        // Generate nama secara acak
        $random_nama_depan = array_rand($nama_depan);
        $random_nama_belakang = array_rand($nama_belakang);
        $random_kata_sambung = array_rand($kata_sambung);

        $ul = user::where('pos_id', $request->id)->where('position',1)->first();
        if($ul){
            $notify[] = ['error', 'Add left User Failed'];
            return response()->json($notify);
        }

        $gnl = GeneralSetting::first();

        $user = new User();
        $user->no_bro       = generateUniqueNoBro();
        $user->ref_id       = 153;
        $user->plan_id      = 1;
        $user->pos_id       = $request->id;
        $user->position     = 1;
        $user->firstname    = $nama_depan[$random_nama_depan];
        $user->lastname     = $nama_belakang[$random_nama_belakang];
        $user->email        = $nama_depan[$random_nama_depan].$nama_belakang[$random_nama_belakang].'@gmail.com';
        $user->password     = Hash::make('123456');
        $user->username     = $nama_depan[$random_nama_depan].$nama_belakang[$random_nama_belakang];
        // $user->ref_id       = $userCheck->id;
        $user->mobile       = '62123456789';
        $user->address      = [
            'address' => '',
            'state' => '',
            'zip' => '',
            'country' => '',
            'city' => ''
        ];
        $user->status = 1;
        $user->is_kyc = 2;
        $user->ev = 1;
        $user->sv = $gnl->sv ? 0 : 1;
        $user->ts = 0;
        $user->tv = 1;
        $user->save();

        $user_extras = new UserExtra();
        $user_extras->user_id = $user->id;
        $user_extras->save();
        updatePaidCount2($user->id);

        $ui = user::where('id',$request->id)->first();
        if (str_contains($ui->email, '+')) { 
            $uis = user::where('username',strtok($ui->username, '_'))->first();
            $uis->is_matriks = 0;
            $uis->save();
        }else{
            $ui->is_matriks = 0;
            $ui->save();
        }

        adminlog(Auth::guard('admin')->user()->id,'Add Left User '.$user->username);

        $notify[] = ['success', 'Add left User Successfully'];
        return response()->json($notify);
    }

    public function snipperRight(Request $request){
        // dd($request->all());
        // Daftar nama depan
        $nama_depan = array(
            "Adi", "Bayu", "Cinta", "Dian", "Edi", "Farida", "Gatot", "Hasan", "Irma", "Jaya",
            "Kusuma", "Lina", "Mira", "Nita", "Oktavia", "Putri", "Rika", "Sari", "Tri", "Umi",
            "Vera", "Wahyu", "Yanto", "Zainab", "Bagus", "Chandra", "Dewi", "Erwin", "Fandi",
            "Gita", "Hadi", "Intan", "Joko", "Kartika", "Lutfi", "Mega", "Nando", "Oscar",
            "Puspita", "Rizky", "Sita", "Tomi", "Umi", "Vino", "Widi", "Yuli", "Zaki"
        );

        // Daftar nama belakang
        $nama_belakang = array(
            "Anwar", "Basuki", "Cahyadi", "Dewi", "Efendi", "Fitriani", "Gunawan", "Hidayat",
            "Irawan", "Junaedi", "Kurnia", "Lubis", "Maulana", "Nasution", "Octaviani",
            "Puspitasari", "Rachmawati", "Suryadi", "Tari", "Utami", "Vania", "Wibisono",
            "Yulianti", "Zulfikar", "Aulia", "Bintang", "Cahya", "Dharma", "Eka", "Febri",
            "Gani", "Hariyanto", "Indra", "Juwita", "Kurniawan", "Larasati", "Mulyadi", "Nanda",
            "Octavia", "Prabowo", "Ratih", "Sasmita", "Teguh", "Umar", "Viona", "Wita", "Yuni",
            "Zahra"
        );

        // Daftar kata sambung
        $kata_sambung = array(
            "", "", "", "", "", "", "", "", "", "", "bin", "binti", "putra", "putri"
        );

        // Generate nama secara acak
        $random_nama_depan = array_rand($nama_depan);
        $random_nama_belakang = array_rand($nama_belakang);
        $random_kata_sambung = array_rand($kata_sambung);

        $ul = user::where('pos_id', $request->id)->where('position',2)->first();
        if($ul){
            $notify[] = ['error', 'Add left User Failed'];
            return response()->json($notify);
        }

        $gnl = GeneralSetting::first();

        $user = new User();
        $user->no_bro       = generateUniqueNoBro();
        $user->ref_id       = 153;
        $user->plan_id      = 1;
        $user->pos_id       = $request->id;
        $user->position     = 2;
        $user->firstname    = $nama_depan[$random_nama_depan];
        $user->lastname     = $nama_belakang[$random_nama_belakang];
        $user->email        = $nama_depan[$random_nama_depan].$nama_belakang[$random_nama_belakang].'@gmail.com';
        $user->password     = Hash::make('123456');
        $user->username     = $nama_depan[$random_nama_depan].$nama_belakang[$random_nama_belakang];
        // $user->ref_id       = $userCheck->id;
        $user->mobile       = '62123456789';
        $user->address      = [
            'address' => '',
            'state' => '',
            'zip' => '',
            'country' => '',
            'city' => ''
        ];
        $user->status = 1;
        $user->is_kyc = 2;
        $user->ev = 1;
        $user->sv = $gnl->sv ? 0 : 1;
        $user->ts = 0;
        $user->tv = 1;
        $user->save();

        $user_extras = new UserExtra();
        $user_extras->user_id = $user->id;
        $user_extras->save();
        updatePaidCount2($user->id);

        $ui = user::where('id',$request->id)->first();
        if (str_contains($ui->email, '+')) { 
            $uis = user::where('username',strtok($ui->username, '_'))->first();
            $uis->is_matriks = 0;
            $uis->save();
        }else{
            $ui->is_matriks = 0;
            $ui->save();
        }
        adminlog(Auth::guard('admin')->user()->id,'Add Right User '.$user->username);

        $notify[] = ['success', 'Add Right User Successfully'];
        return response()->json($notify);
    }


    public function userMatriks(){
        $page_title = 'User Matriks Potensial';
        $empty_message = 'No user found';
        $users = User::where('is_matriks','1')->where('bro_qty','>=',14)->paginate(getPaginate());
        $listplan = Plan::all();
        return view('admin.users.matriks', compact('page_title', 'empty_message', 'users','listplan'));
    }

    public function setReseller(Request $request,$id){
        $user = User::findOrFail($id);
        $request->validate([
            'bro' => 'required',
        ]);

        $user->no_bro = $request->bro;
        $user->plan_id = 1;
        $user->save();

        $notify[] = ['success', 'Set Reseller User Successfully'];
        return back()->withNotify($notify);
    }

    public function addCounting(Request $request,$id){
        $user = User::findOrFail($id);
        $request->validate([
            'qty' => 'required',
        ]);

        autoPlacement($user->id, $request->qty);

        $notify[] = ['success', 'Add Counting User Successfully'];
        return back()->withNotify($notify);
    }


}

