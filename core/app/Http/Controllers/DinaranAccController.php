<?php

namespace App\Http\Controllers;

use App\Models\dinaran_acc;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;

class DinaranAccController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        if (env('APP_ENV') != 'production') {
            $url          = 'https://staging.dinaran-gold.com/api/v1/register'; // for development mode
        }else{
            $url          = 'https://dinaran-gold.com/api/v1/register'; // for production mode
        }           

        $data['body'] = array(
            "name"=>$request->name,
            "email"=>$request->email,
            "phone"=>$request->phone,
            "request"=>"register"
        ); 
        $data['timestamp'] = date('YmdHis'); 
        // $data['timestamp'] = '20210609173600'; 
        $data['method'] = 'POST'; 
        // $data['phone'] = '081282174631';
        $data['phone'] = env('DINARAN_PHONE');
        
        $apikey = env('DINARAN_API_KEY');
        // $apikey = 'c24763ab-c016-406e-94e3-b020bbc5f2cf';
        // $apikey = 'f9c272bf-b1c1-44f3-85c9-27e0cfeab5c5';
        // $apikey = '738abacc-c62e-4749-9a6c-cf79dd9933b2';
        
        $jsonBody = json_encode($data['body'], JSON_UNESCAPED_SLASHES);
        $requestBody = strtolower(hash('sha256', $jsonBody)); 
        $stringToSign = $data['method'] . ':' . $requestBody. ':' . $apikey . ':' . $data['timestamp'];
        $signature = hash_hmac('sha256', $stringToSign, $apikey);
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>json_encode($data['body']),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'timestamp: '.$data['timestamp'],
            'phone: '.$data['phone'],
            'signature: '.$signature
          ),
        ));
        $err = curl_error($curl);
        $ret = curl_exec($curl);
        
        curl_close($curl);
        // dd($ret);
        if($err) {
                    $notify[] = ['error', 'error'];
                    return back()->withNotify($notify);
                } else {
                    $ret = json_decode($ret);
                    // dd(json_encode($ret->message->email));
                    if (isset($ret->status)) {
                        # code...
                        if($ret->status == 200) {
                            $din =  new dinaran_acc();
                            $din->user_id = auth()->user()->id;
                            $din->name = $request->name;
                            $din->email = $request->email;
                            $din->phone = $request->phone;
                            $din->save();

                            $notify[] = ['success', 'Dinaran Account Registered successfully.'];
                            return back()->withNotify($notify);
                        } else {
                            // echo $ret;
                            $notify[] = ['error', json_encode($ret->message)]; 
                            return back()->withNotify($notify);
                        }
                    }else {
                        // echo $ret;
                        $notify[] = ['error', json_encode($ret->message)];
                        return back()->withNotify($notify);
                    }
                }

        
    }


    public function withdraw(Request $request){
        // dd('ok');
        $request->validate([
            'amount' => 'required',
        ]);

        if (getAmount($request->amount) > auth()->user()->balance) {
            $notify[] = ['error', 'Your Request Amount is Larger Then Your Current Balance.'];
            return back()->withNotify($notify);
        }


        if (env('APP_ENV') != 'production') {
            $url          = 'https://staging.dinaran-gold.com/api/v1/execute-buy'; // for development mode
        }else{
            $url          = 'https://dinaran-gold.com/api/v1/execute-buy'; // for production mode
        } 

        $trx = getTrx();

        $data['body'] = array(
            'phone' => $request->phone,
            'amount' =>  $request->amount,
            'ref_id' =>  $trx,
            'request' => 'buy'
        ); 
        
        
        
        $data['timestamp'] = date('YmdHis'); 
        $data['method'] = 'POST'; 
        $data['phone'] = env('DINARAN_PHONE');
        
        $apikey = env('DINARAN_API_KEY');
        
        $jsonBody = json_encode($data['body'], JSON_UNESCAPED_SLASHES);
        $requestBody = strtolower(hash('sha256', $jsonBody)); 
        $stringToSign = $data['method'] . ':' . $requestBody. ':' . $apikey . ':' . $data['timestamp'];
        $signature = hash_hmac('sha256', $stringToSign, $apikey);
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>json_encode($data['body']),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'timestamp: '.$data['timestamp'],
                'phone: '.$data['phone'],
                'signature: '.$signature
            ),
        ));
        $err = curl_error($curl);
        $ret = curl_exec($curl);
        
        curl_close($curl);
        // dd($ret);
        if($err) {
                    $notify[] = ['error', 'error'];
                    return back()->withNotify($notify);
                } else {
                    $ret = json_decode($ret);
                    // dd(json_encode($ret->message->email));
                    if (isset($ret->status)) {
                        # code...
                        if($ret->status == 200) {
                            $user = User::where('id',auth()->user()->id)->first();
                            $user->balance -= $request->amount;
                            $user->save();

                            $transaction = new Transaction();
                            $transaction->user_id = auth()->user()->id;
                            $transaction->amount = getAmount($request->amount);
                            $transaction->post_balance = getAmount(auth()->user()->balance - $request->amount);
                            $transaction->charge = '';
                            $transaction->trx_type = '-';
                            $transaction->details = getAmount($request->amount) . ' ' . 'IDR' . ' Withdraw to Dinaran';
                            $transaction->trx =   $trx;
                            $transaction->save();

                            $notify[] = ['success', 'Withdraw to Dinaran successfully.'];
                            return back()->withNotify($notify);
                        } else {
                            // echo $ret;
                            $notify[] = ['error', json_encode($ret->message)]; 
                            return back()->withNotify($notify);
                        }
                    }else {
                        // echo $ret;
                        $notify[] = ['error', json_encode($ret->message)];
                        return back()->withNotify($notify);
                    }
                }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\dinaran_acc  $dinaran_acc
     * @return \Illuminate\Http\Response
     */
    public function show(dinaran_acc $dinaran_acc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\dinaran_acc  $dinaran_acc
     * @return \Illuminate\Http\Response
     */
    public function edit(dinaran_acc $dinaran_acc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\dinaran_acc  $dinaran_acc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dinaran_acc $dinaran_acc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\dinaran_acc  $dinaran_acc
     * @return \Illuminate\Http\Response
     */
    public function destroy(dinaran_acc $dinaran_acc)
    {
        //
    }
}
