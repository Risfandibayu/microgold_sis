<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Selling Integrated System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link  href="{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}" rel="icon">
  <link  href="{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <!-- <link
    href="https://fonts.googleapis.com/css2?family=DM+Serif+Display:ital@0;1&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    rel="stylesheet"> -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link
    href="https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('assets/new_landing2')}}/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{asset('assets/new_landing2')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{asset('assets/new_landing2')}}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{asset('assets/new_landing2')}}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{asset('assets/new_landing2')}}/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{asset('assets/new_landing2')}}/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{asset('assets/new_landing2')}}/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('assets/new_landing2')}}/css/style.css" rel="stylesheet">
  <link href="{{asset('assets/new_landing2')}}/css/product.css" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
  <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
    integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />



  <!-- =======================================================
  * Template Name: Arsha
  * Updated: Sep 18 2023 with Bootstrap v5.3.2
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  <style>


  </style>
</head>

<body>

  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center justify-content-start">
      <!-- Brand Logo -->

      <div class="col-11 col-md-2">
        <!-- <h1 class="logo spase-nav"><a href="index.html">Arsha</a></h1> -->
        <a href="" class="logo spase-nav"><img src="{{getImage(imagePath()['logoIcon']['path'] .'/logo.png')}}" alt="" class=""></a>
      </div>
      <div class="col-1 col-md-8">


        <!-- Navbar Menu -->
        <nav id="navbar" class="navbar">
          <ul>
            <li><a class="nav-link scrollto active" href="#hero">Beranda</a></li>
            <li><a class="nav-link scrollto" href="#tentang">Tentang</a></li>
            <li><a class="nav-link scrollto" href="#fitur">Fitur</a></li>
            <li><a class="nav-link scrollto" href="#faq">FAQ</a></li>
            <li><a class="nav-link scrollto" href="#kontak">Kontak Kami</a></li>
            <hr>
            <div class="text-center b-lr-m justify-content-center">
                @if (!Auth::check())
                <a class="btn btn-block btn-register" style="justify-content: center;color: white;" href="{{route('user.register')}}">Daftar
                    Gratis</a>
                <a class="btn btn-block btn-login" style="justify-content: center;" href="{{route('user.login')}}">Masuk</a>
                </div>
                @else
                <a class="btn btn-block btn-register" style="justify-content: center;color: white;" href="{{url('/user/dashboard')}}">Dashboard</a>
                @endif
          </ul>
          <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->
      </div>
      <div class="col-md-3 b-lr row">
        @if (!Auth::check())
        <div class="col-6 row">
          <a class="btn btn-block btn-login" href="{{route('user.login')}}">Masuk</a>
        </div>
        <div class="col-6 row">
          <a class="btn btn-block btn-register " href="{{route('user.register')}}">Daftar Gratis</a>
        </div>
        @else
        <div class="col-12 row">
            <a class="btn btn-block btn-register" href="{{url('/user/dashboard')}}">Dashboard</a>
          </div>
        @endif
      </div>
    </div>
  </header><!-- End Header -->


  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-1" data-aos="fade-up"
          data-aos-delay="200">
          <h1 class="mb-4">Mimpi Anda Menjadi Financial Freedom Semakin Nyata!</h1>
          <h3 class="mb-4"><strong>Social Network</strong> yang simple untuk dapatkan extra income dengan cara share
            link. Ada <strong>100.000+</strong> Promotor dan Micro Influencer yang memanfaatkan tools dan peluang ini!
          </h3>
          <div class="d-flex justify-content-center justify-content-lg-start mb-4 row col-md-8">
            <div class="col-12 col-md-5 mb-2 mb-md-0">
              <a href="{{route('user.register')}}" class="btn-lihat-produk scrollto">Daftar Gratis</a>
            </div>
            <div class="col-12 col-md-7">
              <a href="#fitur" class="btn-get-started scrollto">Lihat Cara Kerjanya</a>
            </div>
          </div>
        </div>
        <div class="col-lg-6 order-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="{{asset('assets/new_landing2')}}/img/1.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

    <section id="tentang" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title row justify-content-center">
          <h2 class="mb-3">Kenalin, SIS.
            <br>
            (Selling Integrated System)
          </h2>
          <div class="col-md-8">
            <p>Hanya pembeli yang membeli dari anda, yang mempercayai Anda.</p>
          </div>
        </div>

        <div class="row">
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new_landing2')}}/img/Heart.png" alt=""></div>
              <h4><a href="">Deal Lebih Cepat</a></h4>
              <p>Hanya menggunakan link untuk menjual produk yang tersedia pada platform (Selling Integrated System)
                SIS.</p>
            </div>
          </div>
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new_landing2')}}/img/Wallet.png" alt=""></div>
              <h4><a href="">Income Lebih Banyak</a></h4>
              <p>Setiap penjualan yang terjadi pada link yang anda bagikan, anda berhak mendapatkan komisi terhadap
                penjualan produk tersebut.</p>
            </div>
          </div>
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new_landing2')}}/img/Bag.png" alt=""></div>
              <h4><a href="">Banyak Produk Untuk di Jual</a></h4>
              <p>Terdapat banyak varian produk yang dapat anda pasarkan, temukan kebutuhan pelanggan anda segera dengan
                platform ini.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <section id="fitur" class="services2">
      <div class="container" data-aos="fade-up">

        <div class="section-title row justify-content-center">
          <h2 class="mb-3">Mimpi Financial Freedom Anda <br> Dimulai Dari Sini
          </h2>
          <div class="col-md-8">
            <p>Sekarang Anda dapat menfaatkan pada platform ini.</p>
          </div>
        </div>

        <div class="row">
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new_landing2')}}/img/Featured icon.png" alt=""></div>
              <h4><a href="">Daftar Akun</a></h4>
              <p>Pengguna mendaftar atau membuat akun dengan tujuan mengakses dan menggunakan platform Selling
                Integrated System (SIS) yang terintegrasi untuk menjual produk atau layanan secara efektif & efisien.
              </p>
            </div>
          </div>
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new_landing2')}}/img/Featured icon-1.png" alt=""></div>
              <h4><a href="">Bagikan Link Produk</a></h4>
              <p>Pengguna dapat membagikan link produk melalui berbagai saluran seperti pesan teks, email, media sosial,
                atau di situs web sendiri. Ketika seseorang mengklik link tersebut, mereka akan diarahkan ke halaman
                produk yang di tuju.</p>
            </div>
          </div>
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new_landing2')}}/img/Featured icon-2.png" alt=""></div>
              <h4><a href="">Dapatkan Penghasilan</a></h4>
              <p>Pengguna dapat menghasilkan fee Rp 10.000 pada sebaran link produk yang menghasilkan penjualan produk
                pada platform Selling Integrated System (SIS).</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <section id="clients" class="bg-client section-bg">
      <div class="clients">
        <div class="container">
          <div class="section-title row justify-content-center">
            <div class="col-md-6">
              <h2 class="mb-3">100.000+ Promotor dan Micro Influencer Mempercayai SIS
              </h2>
            </div>
            <div class="col-md-8">
              <p>Beberapa mitra besar kami.</p>
            </div>
          </div>

          <div class="row justify-content-center" data-aos="zoom-in">

            <div class="col-lg-2 col-md-4 col-12 d-flex align-items-center justify-content-center">
              <img src="{{asset('assets/new_landing2')}}/img/Dinaran.png" class="img-fluid" alt="">
            </div>

            <div class="col-lg-2 col-md-4 col-12 d-flex align-items-center justify-content-center">
              <img src="{{asset('assets/new_landing2')}}/img/Microgold.png" class="img-fluid" alt="">
            </div>
            <div class="col-lg-2 col-md-4 col-12 d-flex align-items-center justify-content-center">
              <img src="{{asset('assets/new_landing2')}}/img/Filigrana.png" class="img-fluid" alt="">
            </div>

            <div class="col-lg-2 col-md-4 col-12 d-flex align-items-center justify-content-center">
              <img src="{{asset('assets/new_landing2')}}/img/Bossman.png" class="img-fluid" alt="">
            </div>

            <div class="col-lg-2 col-md-4 col-12 d-flex align-items-center justify-content-center">
              <img src="{{asset('assets/new_landing2')}}/img/Garputala.png" class="img-fluid" alt="">
            </div>

          </div>

        </div>
      </div>
    </section><!-- End Cliens Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq bg-white">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Frequently Asked Questions</h2>
          <p>Semua yang perlu Anda ketahui tentang SIS (Selling Integrated System).</p>
        </div>

        <div class="faq-list">
          <ul>
            <li data-aos="fade-up" data-aos-delay="100">
              <a data-bs-toggle="collapse" class="collapse" data-bs-target="#faq-list-1">Apa Itu SIS (Selling Integrated System) ? <i
                  class="bx bx-chevron-down icon-show"></i></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-1" class="collapse show" data-bs-parent=".faq-list">
                <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem inventore reprehenderit praesentium eligendi laboriosam sequi deserunt deleniti dolorem quisquam impedit, animi sint quasi similique ad quae asperiores reiciendis iste. Dolorem?
                </p>
              </div>
            </li>

          </ul>
        </div>

      </div>
    </section><!-- End Frequently Asked Questions Section -->

    <section id="bergabung" class="bergabung">
        <div class="container" data-aos="fade-up">
          <div class="section-title row justify-content-center">
            <div class="col-10 col-md-6">
              <h2 class="mb-3">Mulai Mendaftar Gratis
              </h2>
            </div>
            <div class="col-md-8">
              <p>Bergabunglah dengan 100.000+ Promotor dan Micro Influncer.</p>
            </div>
          </div>

          <div class="row justify-content-center text-center">
            <div class="col-12 col-md-7">
              <a href="{{route('user.register')}}" class="btn-bergabung scrollto">Daftar Sekarang Gratis</a>
            </div>
          </div>

        </div>
    </section>

    <!-- ======= Contact Section ======= -->
    <!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row full-width-row">

          <div class="col-12 col-lg-6 col-md-5 footer-contact">
            <!-- <h3>Arsha</h3> -->
            <img src="{{asset('assets/new_landing2')}}/img/logo_filigrana.png" alt="" class="img-fluid logo-footer ">
            <p class="pb-3">STC SENAYAN LANTAI 3 NO. 167B. JALAN ASIA AFRIKA PINTU IX SENAYAN</p>
            <p>
              Desa/Kelurahan Gelora, Kec. Tanah Abang <br>
              Kota Adm. Jakarta Pusat 10270, Indonesia<br><br>
              <!-- <strong>Telepon:</strong> +62 812 8217 4631<br> -->
              <!-- <strong>Email:</strong> ptsakabhumiaurum@gmail.com <br> -->
            </p>
          </div>
          

          <div class="col-6 col-lg-2 col-md-3 footer-links">
            <h4>Tautan Berguna</h4>
            <ul>
              <li> <a href="#hero">Beranda</a></li>
              <li> <a href="#about">Tentang</a></li>
              <li> <a href="#fitur">Fitur</a></li>
              <li> <a href="#faq">FAQ</a></li>
              <li> <a href="">Kontak
                  Kami</a></li>
            </ul>
          </div>
          <div class="col-6 col-lg-4 col-md-4 footer-links">
            <h4>Layanan Pengaduan Konsumen</h4>
            <p>(Ditjen PKTN) Direktorat Jenderal Perlindungan Konsumen dan Tertib Niaga <br><br>
              Kementerian Perdagangan Republik Indonesia <br><br>
              Telp : +62 853 1111 1010</p>
          </div>

          <!-- <div class="col-12 col-lg-4 col-md-6 footer-links">
            <h4>Disclaimer:</h4>
            <p>PT Saka Bhumi Aurum adalah perusahaan produsen dan penjual produk emas fisik ukuran mikro (0.01 gr, 0.02
              gr, 0.05 gr) dan PT Saka Bhumi Aurum bukan merupakan bagian atau mitra dari perusahaan Investasi Emas
              Online seperti (Lakuemas, Dinaran, Tamasia, Indogold) dan perusahaan sejenis lainnya.
              <br>
              <br>
              PT Saka Bhumi Aurum tidak memiliki media informasi ataupun media sosial apapun, perusahaan hanya memiliki
              website resmi dengan alamat https://microgold.fund
              <br>
              <br>
              Perusahaan tidak bertanggung jawab atas informasi diluar dari website resmi kami tersebut.
            </p>
          </div> -->
        </div>
        <hr>
      </div>
    </div>
    <div class="container footer-bottom clearfix ">
      <!-- <hr> -->
      <div class="copyright">
        &copy; Copyright Filigrana 2023. All Rights Reserved By PT Suwasa Aji Hemas
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
      class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('assets/new_landing2')}}/vendor/aos/aos.js"></script>
  <script src="{{asset('assets/new_landing2')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{asset('assets/new_landing2')}}/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{asset('assets/new_landing2')}}/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="{{asset('assets/new_landing2')}}/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="{{asset('assets/new_landing2')}}/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="{{asset('assets/new_landing2')}}/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('assets/new_landing2')}}/js/main.js"></script>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <!-- Mengimpor library Owl Carousel JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
  <script>
    $(document).ready(function () {
      var owl = $(".owl-carousel");

      // owl.owlCarousel({
      //   margin: 10,
      //   autoWidth: false,
      //   nav: false,
      //   items: 4,
      //   navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
      //     '<i class="fa fa-angle-right" aria-hidden="true"></i>'
      //   ]
      // })

      owl.owlCarousel({
        autoPlay: 3000,
        margin: 30,
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3],
        center: true,
        nav: false,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
          '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ],
        loop: true,
        responsive: {
          0: {
            items: 1,
            nav: false
          },
          600: {
            items: 2,
            nav: false
          },
          1000: {
            items: 4,
            nav: false,
          }
        }

      });
    });
  </script>
</body>

</html>