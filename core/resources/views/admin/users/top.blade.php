@extends('admin.layouts.app')
@section('panel')
    <div class="row">
        <div class="col-lg-12">
            <div class="card b-radius--10 ">
                <div class="card-body p-0">
                    <div class="table-responsive--md  table-responsive">
                        <table class="table table--light style--two">
                            <thead>
                            <tr>
                                <th scope="col">@lang('#')</th>
                                <th scope="col">@lang('User')</th>
                                <th scope="col">@lang('Email')</th>
                                <th scope="col">@lang('Username')</th>
                                <th scope="col">@lang('Phone')</th>
                                <th scope="col">@lang('BRO Joined')</th>
                                <th scope="col">@lang('BRO Left')</th>
                                <th scope="col">@lang('BRO Right')</th>
                                {{-- <th scope="col">@lang('Joined At')</th> --}}
                                <th scope="col">@lang('Action')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($users as $user)
                            <tr
                            @if ( $users->firstItem()+$loop->index == 1)
                            style="background-color: #ffd7001a;"
                            @elseif ( $users->firstItem()+$loop->index == 2)
                            style="background-color: #c0c0c02e;"
                            @elseif ( $users->firstItem()+$loop->index == 3)
                            style="background-color: #cd7f3226;"
                            @endif
                            >
                                <td data-label="@lang('#')">
                                    {{-- {{ $users->firstItem()+$loop->index }} --}}
                                    @if ( $users->firstItem()+$loop->index == 1)
                                    <button class="btn" style="background-color: gold">1</button>
                                    @elseif ( $users->firstItem()+$loop->index == 2)
                                    <button class="btn" style="background-color: silver">2</button>
                                    @elseif ( $users->firstItem()+$loop->index == 3)
                                    <button class="btn" style="background-color: #cd7f32">3</button>
                                    @else
                                    {{ $users->firstItem()+$loop->index }}
                                    @endif
                                </td>
                                <td data-label="@lang('User')">
                                    <div class="user">
                                        <div class="thumb">
                                            <img src="{{ getImage(imagePath()['profile']['user']['path'].'/'.$user->image,imagePath()['profile']['user']['size'])}}" alt="@lang('image')">
                                        </div>
                                        <span class="name">{{$user->fullname}}</span>
                                    </div>
                                </td>
                                <td data-label="@lang('Email')">{{ $user->email }}</td>

                                <td data-label="@lang('Username')"><a href="{{ route('admin.users.detail', $user->id) }}">{{ $user->username }}</a></td>
                                <td data-label="@lang('Phone')">{{ $user->mobile }}</td>
                                <td data-label="@lang('BRO Joined')">
                                    <strong>{{ $user->bro }}</strong> BRO
                                </td>
                                <td data-label="@lang('BRO Left')">
                                    <strong>{{ $user->bro_left }}</strong> BRO
                                </td>
                                <td data-label="@lang('BRO Right')">
                                    <strong>{{ $user->bro_right }}</strong> BRO
                                </td>
                                {{-- <td data-label="@lang('Joined At')">{{ showDateTime($user->created_at) }}</td>  --}}
                                <td data-label="@lang('Action')">
                                    <a href="{{ route('admin.users.detail', $user->id) }}" class="icon-btn" data-toggle="tooltip" data-original-title="@lang('Details')">
                                        <i class="las la-desktop text--shadow"></i>
                                    </a>
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                                </tr>
                            @endforelse

                            </tbody>
                        </table><!-- table end -->
                    </div>
                </div>
                <div class="card-footer py-4">
                    {{ paginateLinks($users) }}
                </div>
            </div><!-- card end -->
        </div>


    </div>
@endsection



@push('breadcrumb-plugins') 
<div class="row">
    
    <div class="col-md-12 col-12">

        <form action="" method="GET" class="form-inline float-sm-right bg--white mr-2">
            <div class="input-group has_append">
                <input type="text" name="search" class="form-control" placeholder="@lang('Username or email')" value="{{ $search ?? '' }}">
                
                <div class="input-group-append">
                    <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>

        <form action="" method="GET" class="form-inline float-sm-right bg--white">
            <div class="input-group has_append">
                <select name="type" id="type" class="from-select">
                    <option selected hidden>Filter By Leader type</option>
                    <option {{ $type == 0 ? 'selected' : '' }} value="0">By Total</option>
                    <option {{ $type == 1 ? 'selected' : '' }} value="1">Leader (51:51)</option>
                    <option {{ $type == 2 ? 'selected' : '' }} value="2">Marshal (750:750)</option>
                    <option {{ $type == 3 ? 'selected' : '' }} value="3">Centurion (1500:1500)</option>
                </select>
                <div class="input-group-append">
                    <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
@endpush
