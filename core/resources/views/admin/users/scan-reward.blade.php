@extends('admin.layouts.app')

@section('panel')
    <div class="row">
        <div class="col-lg-6 col-12">
            <div class="card b-radius--10 ">
                <div class="card-body p-0">
                <div id="reader"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
{{-- <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script> --}}
<script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script>
    <script>
        function onScanSuccess(decodedText, decodedResult) {
            // console.log(`Code scanned = ${decodedText}`, decodedResult);
            alert(`Code scanned = ${decodedText}`);
        }
        const formatsToSupport = [
        Html5QrcodeSupportedFormats.QR_CODE,
        Html5QrcodeSupportedFormats.CODE_93,
        Html5QrcodeSupportedFormats.CODE_128,
        Html5QrcodeSupportedFormats.UPC_A,
        Html5QrcodeSupportedFormats.UPC_E,
        Html5QrcodeSupportedFormats.UPC_EAN_EXTENSION,
        ];

        function onScanError(errorMessage) {
            // handle on error condition, with error message
        }
        var html5QrcodeScanner = new Html5QrcodeScanner(
            "reader", { fps: 10, qrbox: {width: 800, height: 300}, formatsToSupport: formatsToSupport });
        html5QrcodeScanner.render(onScanSuccess, onScanError);
    </script>
@endpush

