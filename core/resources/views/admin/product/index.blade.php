@extends('admin.layouts.app')

@section('panel')

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-4">
            <div class="card-body p-0">
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Sl')</th>
                                <th scope="col">@lang('Name')</th>
                                <th scope="col">@lang('Thumbnail')</th>
                                <th scope="col">@lang('Price')</th>
                                <th scope="col">@lang('Quantity')</th>
                                <th scope="col">@lang('Status')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse(@$products as $key => $product)
                            <tr>
                                <td data-label="@lang('Sl')">{{ $products->firstItem() + $loop->index }}</td>
                                <td data-label="@lang('Name')">{{ __($product->name) }}</td>
                                <td data-label="@lang('Thumbnail')"> <img src="{{ getImage(imagePath()['products']['path'].'/'.$product->thumbnail,imagePath()['products']['size'])}}" alt="" class="shadow rounded __img"></td>
                                <td data-label="@lang('Price')">{{ __(nb(getAmount($product->price))) }} {{ $general->cur_text }}</td>
                                <td data-label="@lang('Quantity')">{{ __($product->quantity) }}</td>
                                 <td data-label="@lang('Status')">
                                    <div class="__status_switch" data-action="{{ route('admin.product.status.change', $product->id) }}" data-status="{{ $product->status }}" data-id="{{ $product->id }}">
                                    </div>
                                    @if($product->is_reseller == 1)
                                    <span
                                        class="text--small badge font-weight-normal badge--primary">@lang('Reseller')</span>
                                    @else
                                    <span
                                        class="text--small badge font-weight-normal badge--warning">@lang('Public')</span>
                                    @endif
                                </td>
                                <td data-label="@lang('Action')">
                                    <button type="button" class="icon-btn edits" data-toggle="tooltip"
                                        data-id="{{ $product->id }}"
                                        data-name="{{ $product->name }}"
                                        data-stok="{{ $product->quantity }}" data-original-title="Update Stok">
                                        <i class="las la-cloud-download-alt"></i>
                                    </button>
                                    <a  class="icon-btn edit text-white" href="{{ route('admin.product.edit',$product->id) }}" data-toggle="tooltip" data-original-title="Edit">
                                        <i class="la la-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($emptyMessage) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ @$products->links('admin.partials.paginate') }}
            </div>
        </div>
        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Sl')</th>
                                <th scope="col">@lang('Name')</th>
                                <th scope="col">@lang('Thumbnail')</th>
                                <th scope="col">@lang('Price')</th>
                                <th scope="col">@lang('Quantity')</th>
                                <th scope="col">@lang('Status')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse(@$products2 as $key => $product)
                            <tr>
                                <td data-label="@lang('Sl')">{{ $products->firstItem() + $loop->index }}</td>
                                <td data-label="@lang('Name')">{{ __($product->name) }}</td>
                                <td data-label="@lang('Thumbnail')"> <img src="{{ getImage(imagePath()['products']['path'].'/'.$product->thumbnail,imagePath()['products']['size'])}}" alt="" class="shadow rounded __img"></td>
                                <td data-label="@lang('Price')">{{ __(nb(getAmount($product->price))) }} {{ $general->cur_text }}</td>
                                <td data-label="@lang('Quantity')">{{ __($product->quantity) }}</td>
                                 <td data-label="@lang('Status')">
                                    <div class="__status_switch" data-action="{{ route('admin.product.status.change', $product->id) }}" data-status="{{ $product->status }}" data-id="{{ $product->id }}">
                                    </div>
                                    @if($product->is_reseller == 1)
                                    <span
                                        class="text--small badge font-weight-normal badge--primary">@lang('Reseller')</span>
                                    @else
                                    <span
                                        class="text--small badge font-weight-normal badge--warning">@lang('Public')</span>
                                    @endif
                                </td>
                                <td data-label="@lang('Action')">
                                    <button type="button" class="icon-btn edits" data-toggle="tooltip"
                                        data-id="{{ $product->id }}"
                                        data-name="{{ $product->name }}"
                                        data-stok="{{ $product->quantity }}" data-original-title="Update Stok">
                                        <i class="las la-cloud-download-alt"></i>
                                    </button>
                                    <a  class="icon-btn edit text-white" href="{{ route('admin.product.edit',$product->id) }}" data-toggle="tooltip" data-original-title="Edit">
                                        <i class="la la-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($emptyMessage) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ @$products->links('admin.partials.paginate') }}
            </div>
        </div>
    </div>
</div>
<div id="edit-product-stok" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Update Product Stock')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.products.updatestok')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control plan_id" type="hidden" name="id">
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Name')</label>
                            <input type="text" class="form-control name" name="names"  placeholder="Product ABC" readonly>
                        </div>
                        
                    </div>

                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Type')</label>
                            <input type="number" class="form-control weight" name="weight" step="0.001" placeholder="0.001" required>
                        </div>
                    </div> --}}
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Stock')</label>
                            <input type="number" class="form-control stok" name="stok" step="0" placeholder="0" >
                        </div>
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Status')</label>
                            <input type="checkbox" class="t1" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" id="status" data-on="@lang('Active')" data-off="@lang('Inactive')"
                                name="status" checked>
                        </div>
                    </div> --}}
                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Pricing For')</label>
                            <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" data-on="@lang('Reseller')" data-off="@lang('Public')"
                                name="reseller" checked>
                        </div>
                    </div> --}}
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection



@push('breadcrumb-plugins')
<a href="{{ route('admin.product.create') }}" class="btn btn-sm btn--primary box--shadow1 text--small"><i class="las la-plus"></i>@lang('Add
    New')</a>

@endpush

@push('script')
<script>
    "use strict";
    (function($) {



    })(jQuery);

</script>

<script src="{{ asset('assets/admin/js/status-switch.js') }}"></script>
<script>
    "use strict";
        (function ($) {
            $('.edits').on('click', function () {
                console.log($(this).data('image'));
                var modal = $('#edit-product-stok');
                modal.find('.name').val($(this).data('name'));
                modal.find('.stok').val($(this).data('stok'));
                // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });
        })(jQuery);
</script>


@endpush

@push('style')
<style>
    .__img {
        max-width: 150px;
        border-radius: 5px;
        padding: 5px;
    }

</style>
@endpush
