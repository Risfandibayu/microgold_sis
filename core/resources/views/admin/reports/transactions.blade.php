@extends('admin.layouts.app')

@section('panel')
<div class="row">
    <div class="col-lg-12">
        <div class="card b-radius--10 ">
            <div class="card-body p-0">
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Date')</th>
                                <th scope="col">@lang('TRX')</th>
                                <th scope="col">@lang('Full Name')</th>
                                <th scope="col">@lang('Username')</th>
                                <th scope="col">@lang('Amount')</th>
                                <th scope="col">@lang('Charge')</th>
                                <th scope="col">@lang('Post Balance')</th>
                                <th scope="col">@lang('Post Balance On Hold')</th>
                                <th scope="col">@lang('Detail')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($transactions as $trx)
                            <tr>
                                <td data-label="@lang('Date')">{{ showDateTime($trx->created_at) }}</td>
                                <td data-label="@lang('TRX')" class="font-weight-bold">{{ $trx->trx }}</td>
                                <td data-label="@lang('Full Name')" class="font-weight-bold">{{  @$trx->user->firstname.' '.@$trx->user->lastname}}</td>
                                <td data-label="@lang('Username')"><a
                                        href="{{ route('admin.users.detail', $trx->user_id) }}">{{ @$trx->user->username
                                        }}</a>
                                </td>
                                <td data-label="@lang('Amount')" class="budget">
                                    <strong @if ($trx->trx_type == '+') class="text-success" @else class="text-danger"
                                        @endif>
                                        {{ $trx->trx_type == '+' ? '+' : '-' }} {{ nb($trx->amount) }}
                                        {{ $general->cur_text }}</strong>
                                </td>
                                <td data-label="@lang('Charge')" class="budget">{{ $general->cur_sym }}
                                    {{ nb($trx->charge) }} </td>
                                <td data-label="@lang('Post Balance')">{{ nb($trx->post_balance + 0) }}
                                    {{ $general->cur_text }}</td>
                                <td data-label="@lang('Post Balance On Hold')">{{ nb($trx->post_balance_on_hold + 0) }}
                                    {{ $general->cur_text }}</td>
                                <td data-label="@lang('Detail')">{{ __($trx->details) }}</td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $transactions->links('admin.partials.paginate') }}
            </div>
        </div><!-- card end -->
    </div>
</div>
@endsection


@push('breadcrumb-plugins')
{{-- @if (request()->routeIs('admin.users.transactions'))
<form action="" method="GET" class=" bg--white">
    <div class="input-group has_append">
        <input type="text" name="search" class="form-control" placeholder="@lang('TRX / Username')"
            value="{{ $search ?? '' }}">
        <div class="input-group-append">
            <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>
@else
<form action="{{ route('admin.report.transaction.search') }}" method="GET" class=" bg--white">
    <div class="input-group has_append">
        <input type="text" name="search" class="form-control" placeholder="@lang('TRX / Username')"
            value="{{ $search ?? '' }}">
        <div class="input-group-append">
            <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>
@endif --}}
<div class="row">

    <div class="col-md-10 col-12">

        {{-- <form action="{{ route('admin.report.transaction.search') }}" method="GET"
            class=" bg--white mr-5 ml-5 mt-2 mb-2">
        </form> --}}
        <form action="{{route('admin.report.transaction')}}" method="GET" class=" bg--white ml-2">

            <div class="input-group has_append">
                <input type="text" name="search" class="form-control" placeholder="@lang('Username/BRO No')"
                    value="{{ $search ?? '' }}">
                <input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en"
                    class="datepicker-here bg--white text--black form-control" data-position='bottom right'
                    placeholder="@lang('Min - Max date')" autocomplete="off" value="{{ @$dates }}" readonly>
                <input hidden type="text" name="page" class="form-control" placeholder="@lang('Username or email')"
                    value="{{ $page_title ?? '' }}">
                <div class="input-group-append">
                    <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-2 col-4">
        <form action="{{ route('admin.report.export') }}" method="GET" class="">
            <input hidden type="text" name="search" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $search ?? '' }}">
            <input hidden type="text" name="date" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $dates ?? '' }}">
            <input hidden type="text" name="page" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $page_title ?? '' }}">
            <button class="btn btn--primary" type="submit">Export</button>
        </form>
    </div>
</div>
@endpush
@push('script')
<script src="{{ asset('assets/admin/js/vendor/datepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/vendor/datepicker.en.js') }}"></script>
<script>
    'use strict';
  (function($){
      if(!$('.datepicker-here').val()){
          $('.datepicker-here').datepicker();
      }
  })(jQuery)
</script>
@endpush