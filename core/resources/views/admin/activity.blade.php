@extends('admin.layouts.app')

@section('panel')
    <div class="row">

        <div class="col-lg-12">
            <div class="card b-radius--10 ">
                <div class="card-body p-0">

                    <div class="table-responsive--sm table-responsive">
                        <table class="table table--light style--two">
                            <thead>
                            <tr>
                                <th scope="col">@lang('Date')</th>
                                <th scope="col">@lang('Admin')</th>
                                <th scope="col">@lang('Log')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $log)
                                <tr>
                                    <td data-label="@lang('Date')">{{diffForHumans($log->created_at) }}</td>
                                    <td data-label="@lang('User')">{{$log->admin->name}}</td>
                                    <td data-label="@lang('Log')">{{$log->log}}</td>
                                    
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                                </tr>
                            @endforelse

                            </tbody>
                        </table><!-- table end -->
                    </div>
                </div>
                <div class="card-footer py-4">
                    {{ paginateLinks($items) }}
                </div>
            </div><!-- card end -->
        </div>
    </div>
@endsection