<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Selling Integrated System</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link rel="shortcut icon" type="image/png" href="{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}">

    <!-- Google Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"> -->
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('assets/new_landing')}}/vendor/aos/aos.css" rel="stylesheet">
    <link href="{{asset('assets/new_landing')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('assets/new_landing')}}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="{{asset('assets/new_landing')}}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="{{asset('assets/new_landing')}}/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('assets/new_landing')}}/css/style.css" rel="stylesheet">

    <!-- =======================================================
  * Template Name: SoftLand
  * Updated: May 30 2023 with Bootstrap v5.3.0
  * Template URL: https://bootstrapmade.com/softland-bootstrap-app-landing-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex justify-content-between align-items-center">

            <div class="logo">
                <!-- <h1><a href="index.html">SoftLand</a></h1> -->
                <!-- Uncomment below if you prefer to use an image logo -->
                <a href=""><img src="{{getImage(imagePath()['logoIcon']['path'] .'/logo.png')}}" alt="" class="img-fluid"></a>
                {{-- <a href=""><img src="https://filigrana.id/images/logo_filigrana2.png" alt="" class="img-fluid"></a> --}}
            </div>

            <nav id="navbar" class="navbar">
                <ul>
                    {{-- <li class="dropdown"><a href="#"><span>Layanan</span></a>
                        <ul>
                            <li><a href="#">Drop Down 1</a></li>
                            <li><a href="#">Drop Down 2</a></li>
                            <li><a href="#">Drop Down 3</a></li>
                            <li><a href="#">Drop Down 4</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#"><span>Tentang Kami</span></a>
                        <ul>
                            <li><a href="#">Drop Down 1</a></li>
                            <li><a href="#">Drop Down 2</a></li>
                            <li><a href="#">Drop Down 3</a></li>
                            <li><a href="#">Drop Down 4</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#"><span>Cara Kerja</span></a>
                        <ul>
                            <li><a href="#">Drop Down 1</a></li>
                            <li><a href="#">Drop Down 2</a></li>
                            <li><a href="#">Drop Down 3</a></li>
                            <li><a href="#">Drop Down 4</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#"><span>Program AntiCovid</span></a>
                        <ul>
                            <li><a href="#">Drop Down 1</a></li>
                            <li><a href="#">Drop Down 2</a></li>
                            <li><a href="#">Drop Down 3</a></li>
                            <li><a href="#">Drop Down 4</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#"><span>Promoter</span></a>
                        <ul>
                            <li><a href="#">Drop Down 1</a></li>
                            <li><a href="#">Drop Down 2</a></li>
                            <li><a href="#">Drop Down 3</a></li>
                            <li><a href="#">Drop Down 4</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#"><span>#maucash</span></a>
                        <ul>
                            <li><a href="#">Drop Down 1</a></li>
                            <li><a href="#">Drop Down 2</a></li>
                            <li><a href="#">Drop Down 3</a></li>
                            <li><a href="#">Drop Down 4</a></li>
                        </ul>
                    </li> --}}
                    @if (!Auth::check())
                    <li><a class="" href="{{route('user.login')}}"><span class="btn bg-sec btn-sm">Login</span></a></a>
                    </li>
                    <li><a class="" href="{{route('user.register')}}"><span class="btn bg-sec btn-sm">Register</span></a></a>
                    </li>
                    @else

                    <li><a class="" href="{{url('/user/dashboard')}}">Dashboard</a></li>
                    @endif
                    {{-- <li><a class="" href="contact.html"> <span class="btn btn-danger btn-sm">Login</span></a></li>
                    --}}
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->

        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section class="hero-section" id="hero">

        <!-- <div class="wave">

      <svg width="100%" height="355px" viewBox="0 0 1920 355" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
            <path d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,757 L1017.15166,757 L0,757 L0,439.134243 Z" id="Path"></path>
          </g>
        </g>
      </svg>

    </div> -->

        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 hero-text-image">
                    <div class="row">
                        <div class="col-lg-5 text-center text-lg-start">
                            <h1 data-aos="fade-right">Selling Integrated System (SIS)</h1>
                            <p class="" data-aos="fade-right" data-aos-delay="100"><strong style="font-weight: 700
              ;">Social Network</strong> yang simple untuk dapatkan extra income dengan cara share link!</p>
                            <span class="mb-5" data-aos="fade-right" data-aos-delay="200">
                                Ada <strong>100.000+</strong> Promotor dan Micro Influencer yang memanfaatkan tools dan
                                peluang ini !
                                <br>
                                <br>
                                Mimpi Anda menjadi Financial Freedom <strong>semakin nyata!</strong>
                            </span>
                            <p data-aos="fade-right" data-aos-delay="300" data-aos-offset="-500"><a href="{{route('user.login')}}"
                                    class="btn btn-outline-white mt-5">Get started</a></p>
                        </div>
                        <div class="col-lg-7 iphone-wrap">
                            <img src="{{asset('assets/new_landing')}}/img/1.png" alt="Image" class="phone-2" data-aos="fade-right">
                            {{-- <img src="https://viralmu.id/landing/images/gambar-landing-busu.png" alt="Image"
                                class="phone-2" data-aos="fade-right" data-aos-delay="200"> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= Home Section ======= -->
        <section class="section">
            <div class="container">

                <div class="row justify-content-center text-center mb-5">
                    <div class="col-md-12" data-aos="fade-up">
                        <h2 class="section-heading">Selling Integrated System</h2>
                        <h4>Hanya pembeli yang membeli dari anda, yang mempercayai Anda.</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4" data-aos="fade-up" data-aos-delay="">
                        <div class="feature-1 text-center">
                            <!-- <div class="wrap-icon icon-1"> -->
                            <img src="{{asset('assets/new_landing')}}/img/2.png" class="img-fluid"
                                width="250px" alt="">
                            <!-- </div> -->
                            <h3 class="mb-3">Deal Lebih Cepat</h3>
                            <p>Hanya menggunakan link untuk menjual produk yang tersedia pada platform (Selling
                                Integrated System) SIS.</p>
                        </div>
                    </div>
                    <div class="col-md-4" data-aos="fade-up" data-aos-delay="">
                        <div class="feature-1 text-center">
                            <!-- <div class="wrap-icon icon-1"> -->
                            <img src="{{asset('assets/new_landing')}}/img/3.png" class="img-fluid"
                                width="250px" alt="">
                            <!-- </div> -->
                            <h3 class="mb-3">Income Lebih Banyak</h3>
                            <p>Setiap penjualan yang terjadi pada link yang anda bagikan, anda berhak mendapatkan komisi
                                terhadap penjualan produk tersebut.</p>
                        </div>
                    </div>
                    <div class="col-md-4" data-aos="fade-up" data-aos-delay="">
                        <div class="feature-1 text-center">
                            <!-- <div class="wrap-icon icon-1"> -->
                            <img src="{{asset('assets/new_landing')}}/img/4.png" class="img-fluid"
                                width="250px" alt="">
                            <!-- </div> -->
                            <h3 class="mb-3">Banyak Produk Untuk di Jual</h3>
                            <p>Terdapat banyak varian produk yang dapat anda pasarkan, temukan kebutuhan pelanggan anda
                                segera dengan platform ini.</p>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <section class="section">

            <div class="container">
                <div class="row justify-content-center text-center mb-5" data-aos="fade">
                    <div class="col-md-6 mb-5">
                        {{-- <img src="{{asset('assets/new_landing')}}/img/undraw_svg_1.svg" alt="Image" class="img-fluid"> --}}
                        <img src="{{asset('assets/new_landing')}}/img/5.png" alt="Image" class="img-fluid">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="step h-100">
                            <span class="number">01</span>
                            <h3>Daftar Akun</h3>
                            <p>Pengguna mendaftar atau membuat akun dengan tujuan mengakses dan menggunakan platform Selling Integrated System (SIS) yang terintegrasi untuk menjual produk atau layanan secara efektif & efisien. </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="step h-100">
                            <span class="number">02</span>
                            <h3>Bagikan Link Produk</h3>
                            <p>Pengguna dapat membagikan link produk melalui berbagai saluran seperti pesan teks, email, media sosial, atau di situs web sendiri. Ketika seseorang mengklik link tersebut, mereka akan diarahkan ke halaman produk yang di tuju.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="step h-100">
                            <span class="number">03</span>
                            <h3>Dapatkan Penghasilan</h3>
                            <p>Pengguna dapat menghasilkan fee Rp 10.000 pada sebaran link produk yang menghasilkan penjualan produk pada platform Selling Integrated System (SIS).</p>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        {{-- <section class="section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4 me-auto">
                        <h2 class="mb-4">Seamlessly Communicate</h2>
                        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur at
                            reprehenderit optio,
                            laudantium eius quod, eum maxime molestiae porro omnis. Dolores aspernatur delectus impedit
                            incidunt
                            dolore mollitia esse natus beatae.</p>
                        <p><a href="#" class="btn btn-primary">Download Now</a></p>
                    </div>
                    <div class="col-md-6" data-aos="fade-left">
                        <img src="{{asset('assets/new_landing')}}/img/undraw_svg_2.svg" alt="Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4 ms-auto order-2">
                        <h2 class="mb-4">Gather Feedback</h2>
                        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur at
                            reprehenderit optio,
                            laudantium eius quod, eum maxime molestiae porro omnis. Dolores aspernatur delectus impedit
                            incidunt
                            dolore mollitia esse natus beatae.</p>
                        <p><a href="#" class="btn btn-primary">Download Now</a></p>
                    </div>
                    <div class="col-md-6" data-aos="fade-right">
                        <img src="{{asset('assets/new_landing')}}/img/undraw_svg_3.svg" alt="Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </section> --}}

        <!-- ======= Testimonials Section ======= -->
        {{-- <section class="section border-top border-bottom">
            <div class="container">
                <div class="row justify-content-center text-center mb-5">
                    <div class="col-md-4">
                        <h2 class="section-heading">Review From Our Users</h2>
                    </div>
                </div>
                <div class="row justify-content-center text-center">
                    <div class="col-md-7">

                        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
                            <div class="swiper-wrapper">

                                <div class="swiper-slide">
                                    <div class="review text-center">
                                        <p class="stars">
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill muted"></span>
                                        </p>
                                        <h3>Excellent App!</h3>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea
                                                delectus pariatur, numquam
                                                aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae
                                                deleniti minus animi,
                                                provident voluptates consectetur maiores quos.</p>
                                        </blockquote>

                                        <p class="review-user">
                                            <img src="{{asset('assets/new_landing')}}/img/person_1.jpg" alt="Image"
                                                class="img-fluid rounded-circle mb-3">
                                            <span class="d-block">
                                                <span class="text-black">Jean Doe</span>, &mdash; App User
                                            </span>
                                        </p>

                                    </div>
                                </div><!-- End testimonial item -->

                                <div class="swiper-slide">
                                    <div class="review text-center">
                                        <p class="stars">
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill muted"></span>
                                        </p>
                                        <h3>This App is easy to use!</h3>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea
                                                delectus pariatur, numquam
                                                aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae
                                                deleniti minus animi,
                                                provident voluptates consectetur maiores quos.</p>
                                        </blockquote>

                                        <p class="review-user">
                                            <img src="{{asset('assets/new_landing')}}/img/person_2.jpg" alt="Image"
                                                class="img-fluid rounded-circle mb-3">
                                            <span class="d-block">
                                                <span class="text-black">Johan Smith</span>, &mdash; App User
                                            </span>
                                        </p>

                                    </div>
                                </div><!-- End testimonial item -->

                                <div class="swiper-slide">
                                    <div class="review text-center">
                                        <p class="stars">
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill muted"></span>
                                        </p>
                                        <h3>Awesome functionality!</h3>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea
                                                delectus pariatur, numquam
                                                aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae
                                                deleniti minus animi,
                                                provident voluptates consectetur maiores quos.</p>
                                        </blockquote>

                                        <p class="review-user">
                                            <img src="{{asset('assets/new_landing')}}/img/person_3.jpg" alt="Image"
                                                class="img-fluid rounded-circle mb-3">
                                            <span class="d-block">
                                                <span class="text-black">Jean Thunberg</span>, &mdash; App User
                                            </span>
                                        </p>

                                    </div>
                                </div><!-- End testimonial item -->

                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End Testimonials Section --> --}}

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer class="footer" role="contentinfo">
        <div class="container">
            {{-- <div class="row">
                <div class="col-md-4 mb-4 mb-md-0">
                    <h3>About SoftLand</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea delectus pariatur, numquam
                        aperiam
                        dolore nam optio dolorem facilis itaque voluptatum recusandae deleniti minus animi.</p>
                    <p class="social">
                        <a href="#"><span class="bi bi-twitter"></span></a>
                        <a href="#"><span class="bi bi-facebook"></span></a>
                        <a href="#"><span class="bi bi-instagram"></span></a>
                        <a href="#"><span class="bi bi-linkedin"></span></a>
                    </p>
                </div>
                <div class="col-md-7 ms-auto">
                    <div class="row site-section pt-0">
                        <div class="col-md-4 mb-4 mb-md-0">
                            <h3>Navigation</h3>
                            <ul class="list-unstyled">
                                <li><a href="#">Pricing</a></li>
                                <li><a href="#">Features</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 mb-4 mb-md-0">
                            <h3>Services</h3>
                            <ul class="list-unstyled">
                                <li><a href="#">Team</a></li>
                                <li><a href="#">Collaboration</a></li>
                                <li><a href="#">Todos</a></li>
                                <li><a href="#">Events</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 mb-4 mb-md-0">
                            <h3>Downloads</h3>
                            <ul class="list-unstyled">
                                <li><a href="#">Get from the App Store</a></li>
                                <li><a href="#">Get from the Play Store</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> --}}

            <div class="row justify-content-center text-center">
                <div class="col-md-7">
                    <p class="copyright">&copy; Copyright Filigrana 2023. All Rights Reserved By PT Suwasa Aji Hemas</p>
                </div>
            </div>

        </div>
    </footer>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{asset('assets/new_landing')}}/vendor/aos/aos.js"></script>
    <script src="{{asset('assets/new_landing')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('assets/new_landing')}}/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="{{asset('assets/new_landing')}}/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('assets/new_landing')}}/js/main.js"></script>
    <script>
        $(function () {
        $("#switch-id").change(function () {
            if ($(this).is(":checked")) {
                $(".contentB").show();
                $(".contentA").hide();
            } else {
                $(".contentB").hide();
                $(".contentA").show();
            }
        });
    });
    </script>

</body>

</html>