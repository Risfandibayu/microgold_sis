@extends($activeTemplate . 'user.layouts.app')

@section('panel')
    <div class="container">
        <div class="row  justify-content-center">
            <div class="col-md-12 col-lg-12 col-xl-10 col-xxl-7">
                <div class="card card-deposit text-center">
                    <div class="card-body card-body-deposit card-body">
                        <div class="">
                            {{-- <div class="deposit-thumb">
                                <img class="" src="{{ $data->gateway_currency()->methodImage() }}" />
                            </div> --}}
                            <div class="deposit-content">
                                <ul class="mb-3">
                                    <li>
                                        <table class="table table--light style--two">
                                            <thead>
                                                <tr>
                                                    {{-- <th scope="col">@lang('SL')</th> --}}
                                                    <th scope="col">@lang('Product')</th>
                                                    <th scope="col" width="20%">@lang('Qty')</th>
                                                    {{-- <th scope="col" width="20%">@lang('Weight')</th> --}}
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($devcart as $ex)
                                                <tr>
                                                    {{-- <td data-label="@lang('SL')">{{ $devcart->firstItem()+$loop->index }}</td> --}}
                                                    <td data-label="@lang('Product')">
                                                        {{-- {{ $ex->gold->prod->name }} --}}
                                                        {{-- <div class="row d-flex">
                                                            <div class="book">
                                                                <img src="https://i.imgur.com/2DsA49b.jpg" class="book-img">
                                                            </div>
                                                            <div class="my-auto flex-column d-flex pad-left">
                                                                <h6 class="mob-text">Thinking, Fast and Slow</h6>
                                                            </div>
                                                        </div> --}}
                                                        @if ($ex->gold->is_custom == 0)
                                                        <div class="row d-flex">
                                                            <div class="book">
                                                                <img src="{{ getImage('assets/images/product/'. $ex->gold->prod->image,  null, true)}}"
                                                                    class="book-img">
                                                            </div>
                                                            <div class="my-auto flex-column d-flex pad-left">
                                                                <h6 class="mob-text text-left font-weight-bold ml-2">Product Name : {{
                                                                    $ex->gold->prod->name }} </h6>
                                                                <h6 class="mob-text text-left font-weight-bold ml-2">Type : {{
                                                                    $ex->gold->prod->weight }} gr</h6>
                                                            </div>
                                                        </div>
                                                        {{-- {{ $ex->gold->prod->name }} --}}
                                                        @else
                                                        <div class="row d-flex">
                                                            <div class="book">
                                                                <img src="{{ getImage('assets/images/cproduct/f/'. $ex->gold->cord->front,  null, true)}}"
                                                                    class="book-img">
                                                            </div>
                                                            <div class="my-auto flex-column d-flex pad-left">
                                                                <h6 class="mob-text text-left font-weight-bold ml-2">Product Name : {{
                                                                    $ex->gold->cord->name}}</h6>
                                                                <h6 class="mob-text text-left font-weight-bold ml-2">Type : {{
                                                                    $ex->gold->prod->weight }} gr</h6>
                                                            </div>
                                                        </div>
                                                        {{-- {{ $ex->gold->cord->name}} --}}
                
                                                        @endif
                
                                                    </td>
                                                    <td data-label="@lang('Qty')">
                                                        {{-- {{ $ex->qty }} --}}
                                                        <div class="input-group">
                                                            
                                                            <input type="text" readonly
                                                                class="form-control input-number" value="{{ $ex->qty }} pcs" min="1"
                                                                max="{{$ex->gold->qty}}">
                                                            
                                                        </div>
                                                    </td>
                                                    {{-- <td data-label="@lang('Weight')">
                                                        <div class="input-group">
                                                            
                                                            <input type="text" readonly
                                                                class="form-control input-number" value="{{ $ex->qty * $ex->gold->prod->weight }} gr" min="1"
                                                                max="{{$ex->gold->qty}}">
                                                            
                                                        </div>
                                                    </td> --}}
                                                    
                
                                                </tr>
                                                @empty
                                                <tr>
                                                    <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                                                </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                        
                                    </li>
                                    <li>
                                        @lang('Total Weight'):
                                        <b class="fw-bolder"><span class="">{{$berattotal}} Gr</b>
                                    </li>
                                    <li>
                                        @lang('Total Pieces'):
                                        <b class="fw-bolder"><span class="">{{$keptotal}} Pcs</b>
                                    </li>
                                    <hr>
                                    <li>
                                        @lang('Recipient`s name'):
                                        <b class="fw-bolder"><span class="">{{$alamat->nama_penerima}}</b>
                                    </li>
                                    <li>
                                        @lang('Recipient`s phone number'):
                                        <b class="fw-bolder"><span class="">{{$alamat->no_telp}}</b>
                                    </li>
                                    <li>
                                        @lang('Full Address'):
                                        <b class="fw-bolder"><span class="">{{ Str::limit($alamat->alamat,40) }}</b>
                                    </li>
                                    <li>
                                        @lang('Postal Code'):
                                        <b class="fw-bolder"><span class="">{{$alamat->kode_pos}}</b>
                                    </li>
                                    <li>
                                        @lang('Shipping Cost'):
                                        <b class="fw-bolder"><span class="">Rp. {{nb($ongkir)}}</b>
                                    </li>
                                    
                                </ul>
                                <form action="{{route('user.gold.checkoutconfirm')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="ongkir" value="{{$ongkir}}">
                                    <input type="hidden" name="alamat" value="{{$alamat->id}}">


                                    <button type="submit" class="btn btn--success btn-block py-3 font-weight-bold" onclick="this.form.submit(); this.disabled=true; this.value='Sending…';">@lang('Confirm Delivery')</button>
                                </form>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@stop


