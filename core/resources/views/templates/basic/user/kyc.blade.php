@extends($activeTemplate . 'user.layouts.app')

@section('panel')

    <div class="row mb-none-30">
        

        <div class="col-xl-12 col-lg-12 col-md-12 col-12 mb-30">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-10 border-bottom pb-2">{{auth()->user()->fullname}} @lang('Information')</h5>

                    <form action="{{route('user.submitVerification')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">@lang('First Name') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="text" name="firstname"
                                           value="{{auth()->user()->firstname}}" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label  font-weight-bold">@lang('Last Name') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="text" name="lastname" value="{{auth()->user()->lastname}}" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            @if (auth()->user()->plan_id != 0)
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">@lang('No SP')<span class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="email" value="{{auth()->user()->no_bro}}" readonly>
                                </div>
                            </div>
                            @endif

                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">@lang('Email')<span class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="email" value="{{auth()->user()->email}}" readonly>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label  font-weight-bold">@lang('Mobile Number')<span
                                            class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="text"
                                           value="{{auth()->user()->mobile}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label  font-weight-bold">@lang('Avatar')</label>
                                    <input class="form-control form-control-lg" type="file" accept="image/*"  onchange="loadFile(event)" name="image">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">@lang('Address')<span
                                        class="text-danger">*</span> </label>
                                    <input class="form-control form-control-lg" type="text" name="address"
                                           value="{{auth()->user()->address->address}}" required>
                                    <small class="form-text text-muted"><i
                                            class="las la-info-circle"></i>@lang('House number, street address')
                                    </small>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label font-weight-bold">@lang('City')<span
                                        class="text-danger">*</span></label>
                                    {{-- <input class="form-control form-control-lg" type="text" name="city"
                                           value="{{auth()->user()->address->city}}" required> --}}
                                        <select id="single" name="city" class="js-states form-control form-control-lg" required>
                                            <option value="{{auth()->user()->address->city}}">{{auth()->user()->address->city}}</option>
                                        </select>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">@lang('State')<span
                                        class="text-danger">*</span></label>
                                    {{-- <input class="form-control form-control-lg" type="text" name="state"
                                           value="{{auth()->user()->address->state}}" required> --}}
                                           <select id="single2" name="state" class="js-states form-control form-control-lg" required>
                                            <option value="{{auth()->user()->address->state}}">{{auth()->user()->address->state}}</option>
                                        </select>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">@lang('Zip/Postal')<span
                                        class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="text" name="zip"
                                           value="{{auth()->user()->address->zip}}" required>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">@lang('Country')<span
                                        class="text-danger">*</span></label>
                                    <select name="country" class="form-control form-control-lg" required> @include('partials.country') </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-xl-6 col-md-6 col-12">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">National ID/Passport ID<span
                                        class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" value="{{auth()->user()->nik}}" type="text" name="nik"
                                            required>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-3 col-12">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">National ID/Passport ID Photo<span
                                        class="text-danger">*</span></label>
                                        <input class="form-control form-control-lg" type="file" accept="image/*"  onchange="loadFile(event)" name="ktp" required>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-3 col-12">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">Selfie with National ID/Passport ID<span
                                        class="text-danger">*</span></label>
                                        <input class="form-control form-control-lg" type="file" accept="image/*"  onchange="loadFile(event)" name="selfie" required>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h5 class="card-title mb-10 border-bottom pb-2 pt-4">{{auth()->user()->fullname}} @lang('Bank Account Information')</h5>
                        @if ($rek)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">@lang('Bank Name') <span
                                            class="text-danger">*</span></label>
                                    {{-- <input class="form-control form-control-lg" type="text" name="firstname"
                                        value="{{auth()->user()->firstname}}" required> --}}
                                    <select name="bank_name" id="bank_name" class="form-control form-control-lg" required>
                                        <option value="" hidden selected>-- Pilih Bank --</option>
                                        @foreach ($bank as $item)
                                        <option {{ auth()->user()->userBank->nama_bank == $item['code'] ? 'selected' : '' }} value="{{$item['code']}}">{{$item['name']}}</option>
                                        @endforeach
    
                                    </select>
                                </div>
                            </div>
    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label  font-weight-bold">@lang('Bank Branch City')
                                        <span class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="text" name="kota_cabang" value="{{auth()->user()->userBank->kota_cabang}}"
                                        placeholder="Bank KCP Jakarta" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label  font-weight-bold">@lang('Account Name') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="text" name="acc_name" value="{{auth()->user()->userBank->nama_akun}}"
                                        required placeholder="Account Name">
                                </div>
                            </div>
                            <div class="col-md-6">
    
                                <div class="form-group">
                                    <label class="form-control-label  font-weight-bold">@lang('Account Number') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="text" placeholder="Account Number"
                                        name="acc_number" value="{{auth()->user()->userBank->no_rek}}" required>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="form-control-label font-weight-bold">@lang('Bank Name') <span
                                            class="text-danger">*</span></label>
                                    {{-- <input class="form-control form-control-lg" type="text" name="firstname"
                                        value="{{auth()->user()->firstname}}" required> --}}
                                    <select name="bank_name" id="bank_name" class="form-control form-control-lg" required>
                                        <option value="" hidden selected>-- Pilih Bank --</option>
                                        @foreach ($bank as $item)
                                        <option value="{{$item['code']}}">{{$item['name']}}</option>
                                        @endforeach
    
                                    </select>
                                </div>
                            </div>
    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label  font-weight-bold">@lang('Bank Branch City')
                                        <span class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="text" name="kota_cabang" value=""
                                        placeholder="Bank KCP Jakarta" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label  font-weight-bold">@lang('Account Name') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="text" name="acc_name" value=""
                                        required placeholder="Account Name">
                                </div>
                            </div>
                            <div class="col-md-6">
    
                                <div class="form-group">
                                    <label class="form-control-label  font-weight-bold">@lang('Account Number') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" type="text" placeholder="Account Number"
                                        name="acc_number" value="" required>
                                </div>
                            </div>
                        </div>
                        @endif
                        

                        <div class="row mt-4">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn--primary btn-block btn-lg">@lang('Save Changes')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('breadcrumb-plugins')
    <a href="{{route('user.change-password')}}" class="btn btn--success btn--shadow"><i class="fa fa-key"></i>@lang('Change Password')</a>
@endpush



@push('script')
    <script>
        'use strict';
        (function($){
            $("select[name=country]").val("{{ auth()->user()->address->country }}");
        })(jQuery)

        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };


    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $("#single").select2({
            placeholder: "Example: Sleman",
                    closeOnSelect: false,
                    allowClear: true,
                    delay: 250, // wait 250 milliseconds before triggering the request
                    ajax: {
                        url: "{{ route('city') }}",
                        dataType: "json",
                        data: function(params) {
                            return {
                                search: params.term
                            };
                        },
                        processResults: function(data) {
                            var results = [];
                            $.each(data, function(index, item) {
                                results.push({
                                    id: item.city,
                                    text: item.city,
                                    value: item.city
                                })
                            })
                            return {
                                results: results
                            };
                        }
                    }
          });
    </script>
    <script>
        $("#single2").select2({
            placeholder: "Example: DKI Jakarta",
                    closeOnSelect: false,
                    allowClear: true,
                    delay: 250, // wait 250 milliseconds before triggering the request
                    ajax: {
                        url: "{{ route('province') }}",
                        dataType: "json",
                        data: function(params) {
                            return {
                                search: params.term
                            };
                        },
                        processResults: function(data) {
                            var results = [];
                            $.each(data, function(index, item) {
                                results.push({
                                    id: item.province,
                                    text: item.province,
                                    value: item.province
                                })
                            })
                            return {
                                results: results
                            };
                        }
                    }
          });
    </script>
@endpush
@push('style')
    <style>
        .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 47px !important;
    user-select: none;
    -webkit-user-select: none;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 47px !important;
    }
    </style>
@endpush