@extends($activeTemplate . 'user.layouts.app')

@section('panel')
<div class="row">
    {{-- @dump($product) --}}
    <div class="col-md-12 mb-20">
        <div class="card b-radius--10">
            <div class="card-body p-0">
                <section class="py-5">
                    <div class="container ">
                        <div class="row align-items-center">
                            <div class="col-md-6 zoom"><img class="card-img-top mb-5 mb-md-0" src="{{ getImage('assets/images/products/'. $product->thumbnail,  null, true)}}" alt="..." /></div>
                            <div class="col-md-6">
                                <h1 class="display-5 fw-bolder">{{$product->name}}</h1>
                                <div class="fs-5 mb-5">
                                    <span>Rp. {{nb($product->price)}}</span>
                                </div>
                                <p class="lead">{{$product->deskripsi}}</p>
                                <p class="lead mt-1">Type :  {{nbt($product->weight)}} Gram</p>
                                <p class="lead">Category :  {{$product->cat->name ?? 'N/A'}}</p>
                                <div class="d-flex mt-2">
                                    <button data-id="{{ $product->id }}" data-name="{{ $product->name }}" data-price="{{ $product->price }}"
                                        data-image="{{ getImage('assets/images/products/'. $product->thumbnail,  null, true)}}"
                                        data-toggle="modal" class="btn btn--custom buy" type="button">
                                        <i class="las la-shopping-cart"></i>
                                        Buy
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div><!-- card end -->
    </div>
</div>

<div class="modal fade" id="buy-product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> Confirm Purchase <span id="prod_name"></span> ?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                </div>
                <form method="post" action="{{route('user.product.purchase')}}">
                    {{-- <form method="post"> --}}
                        {{-- <div class="modal-body"> --}}
                            {{-- </div> --}}
                        @csrf
                        <div class="modal-body row">
                            <div class="px-100">
                                <img src="" alt="" id="img" class="img-fluid">
                            </div>
                            <h5 class="text-center col-12"><span id="price"></span> {{$general->cur_text}} / Item</h5>
                            <input type="hidden" name="prices" id="prices" value="">
                            <input type="hidden" name="product_id" id="product_id" value="">
                            <input type="hidden" name="product_name" id="product_name" value="">
                            {{-- <input type="hidden" name="ref" id="product_name" value="{{$ref}}"> --}}
                            <div class="form-group col-6">
                                <label for="">QTY</label>
                                <input class="form-control" type="number" name="qty" id="qty" min="1" placeholder="QTY"
                                    required>
                            </div>
                            <div class="form-group col-6">
                                <label for="">total</label>
                                <input class="form-control" type="number" name="total" id="total" value=""
                                    placeholder="total" disabled>
                                <input class="form-control" type="hidden" name="totals" id="totals" value=""
                                    placeholder="total">
                            </div>
                            {{-- <div class="form-group col-12">
                                <label for="">refferal</label>
                                <input class="form-control" type="text" name="ref" id="ref" value="{{$ref}}"
                                    placeholder="ref" required>
                            </div> --}}
                            {{-- <div class="form-group col-12">
                                <label for="">

                                    <small>
                                        *Disclaimer: Ketika anda klik tombol Buy, maka anda telah setuju terhadap proses transaksi pembelian retail PT Suwasa Aji Hemas (Filigrana), sistem otomatis mengambil saldo pada balance sesuai nominal transaksi anda.
                                    </small>
                                </label>
                                </div> --}}

                            <div class="col-12">
                                <hr>
                            </div>
                            <div class="col-12 row">
                                <div class="col-7">
                                    <label for="">OTP</label>
                                    <input class="form-control" type="number" name="otp" id="otp" min="1" placeholder="OTP"
                                    required>
                                </div>
                                <div class="col-5">
                                    <label for=""></label>
                                    <button class="form-control btn btn--primary mt-2" id="submitButtonId">Request OTP Code</button>
                                    {{-- <form id="otp" action="{{route('user.otp.product')}}" method="post">
                                        @csrf
                                    </form> --}}
                                </div>
                                <div class="form-group col-12" id="alert" hidden>
                                    <label for="">
                                        <small id="message">
                                        </small>
                                    </label>
                                    </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn--danger" data-dismiss="modal"><i
                                    class="fa fa-times"></i>
                                @lang('Close')</button>

                            <button type="submit" class="btn btn--success"><i class="lab la-telegram-plane"></i>
                                @lang('Buy')</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-zoom/1.5.0/jquery.zoom.min.js" integrity="sha512-bhradwVeLNK+x3r23T0xT1lJXmDIxXs6dgB5d80jc/lssPjGTobgXqiqPA2EHjpcs49X1mLsQQvETOxzQc7zag==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function(){
        $(".zoom").zoom();
    });
</script>
<script>
    // $('input[name=qty]').on('keyup change',function() { 
    //     // alert('okl');
    //     $('input[name=total]').val($('input[name=qty]').val() * $('input[name=prices]').val());
    //     $('input[name=totals]').val($('input[name=qty]').val() * $('input[name=prices]').val());
    // });
</script>
<script>
    "use strict";
        (function ($) {
            $('.buy').on('click', function () {
                // console.log($(this).data('name'));
                var modal = $('#buy-product');
                modal.find('#img').attr("src",$(this).data('image'));
                modal.find('#prod_name').html($(this).data('name'));
                modal.find('#prices').val($(this).data('price'));
                modal.find('#price').html($(this).data('price'));
                modal.find('#product_id').val($(this).data('id'));
                modal.find('#product_name').val($(this).data('name'));

                modal.find('#qty').on('keyup change',function() { 
        // alert('okl');
                    modal.find('#total').val(modal.find('#qty').val() * modal.find('#prices').val());
                    modal.find('#totals').val(modal.find('#qty').val() * modal.find('#prices').val());
                });
                // modal.find('.weight').val($(this).data('weight'));
                // var input = modal.find('.image');
                // // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

                // if($(this).data('status')){
                //     modal.find('.toggle').removeClass('btn--danger off').addClass('btn--success');
                //     modal.find('input[name="status"]').prop('checked',true);

                // }else{
                //     modal.find('.toggle').addClass('btn--danger off').removeClass('btn--success');
                //     modal.find('input[name="status"]').prop('checked',false);
                // }

                // modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.modal').on('hidden.bs.modal', function(){
                $(this).find('form')[0].reset();
            });

            $('.custom-order').on('click', function () {
                var modal = $('#custom-order');
                $("#prod_id").change(function() {
                    var selectedItem = $(this).val();
                    var abc = $('option:selected',this).data("price");
                    // alert(abc);
                    modal.find('#ctotal').val("");
                    modal.find('#cqty').val("");
                    modal.find('#cqty').on('keyup change',function() { 
        // // alert('okl');
                    
                        modal.find('#ctotal').val(modal.find('#cqty').val() * abc);
                    });
                });
                // console.log($(".type option:selected").attr('data-price'));
                // modal.find('prod_id').val($(this).data('price'));
        //         modal.find('#qty').on('keyup change',function() { 
        // // alert('okl');
                    
        //             modal.find('#ctotal').val(modal.find('#cqty').val() * modal.find('#cprice').val());
        //         });
                modal.modal('show');
            });

            $('.detailBtn').on('click', function () {
                var modal = $('#detailModal');
                var feedback = $(this).data('admin_feedback');
                modal.find('.withdraw-detail').html(`<p> ${feedback} </p>`);
                modal.modal('show');
            });
            $('.detailOrder').on('click', function () {
                var modal = $('#detail-order');
                console.log($(this).data('status'));
                // var feedback = $(this).data('admin_feedback');
                // modal.find('.withdraw-detail').html(`<p> ${feedback} </p>`);
                modal.find('#imgf').attr("src",$(this).data('front'));
                modal.find('#imgb').attr("src",$(this).data('back'));
                modal.find('#name').val($(this).data('name'));
                modal.find('#id').val($(this).data('id'));
                if ($(this).data('status') === 4) {
                    // console.log('oi');
                    modal.find('#ifront').removeClass("hidden");
                    modal.find('#iback').removeClass("hidden");
                    modal.find('#btnr').removeClass("hidden");
                }else{
                    // modal.find('#ifront').attr("src",false);
                    // console.log('sip');
                    modal.find('#ifront').addClass("hidden");
                    modal.find('#iback').addClass("hidden");
                    modal.find('#btnr').addClass("hidden");
                }
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };
    var loadFilef = function(event) {
            var output = document.getElementById('imgf');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };
    var loadFileb = function(event) {
            var output = document.getElementById('imgb');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };
    var loadFilefadd = function(event) {
            var output = document.getElementById('imgfadd');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };
    var loadFilebadd = function(event) {
            var output = document.getElementById('imgbadd');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };

</script>
<script>
    $("#submitButtonId").click(function() {

        // console.log('s');
        var url = "{{route('user.otp.product')}}"; // the script where you handle the form input.

        $.ajax({
            type: "POST",
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            success: function(data)
            {
                // alert(data.message); // show response from the php script.
                $("#alert").removeAttr('hidden');
                $("#submitButtonId").prop('disabled', true);
                $('#message').html(data.message);
            }
            });

        return false; // avoid to execute the actual submit of the form.
});

</script>

<script>
    
    function CopyToClipboard() {
    // Create an input
        var input = document.createElement('input');
        // Set it's value to the text to copy, the input type doesn't matter
        input.value = "{{route('user.product.detail',[$product->id])}}?ref={{auth()->user()->username}}";
        // add it to the document
        document.body.appendChild(input);
        // call select(); on input which performs a user like selection  
        input.select();
        // execute the copy command, this is why we add the input to the document
        document.execCommand("copy");
        // remove the input from the document
        document.body.removeChild(input);
        alert('Link copied')
    }
</script>
@endpush
@push('breadcrumb-plugins')
<button onclick="CopyToClipboard()"class="btn btn-sm btn--info"><i
    class="fa fa-fw fa-clipboard"></i>Copy Your Link Refferal</button>
<a href="{{ url()->previous() }}" class="btn btn-sm btn--danger"><i
    class="fa fa-fw fa-chevron-left"></i>back</a>
@endpush
@push('breadcrumb-plugins')
@if (auth()->user()->plan_id != 0)
{{-- <a href="{{route('user.request.sprod',[auth()->user()->id])}}" class="btn btn-sm btn--warning req-prod"><i class="fa fa-fw fa-star"></i>@lang('Request Special Product')</a> --}}
@endif
@endpush
@push('style')
<style>
    .hidden {display:none;}
    .zoom {
			display:inline-block;
			position: relative;
		}
		
		/* magnifying glass icon */
		.zoom:after {
			content:'';
			display:block; 
			width:33px; 
			height:33px; 
			position:absolute; 
			top:0;
			right:0;
			background:url(icon.png);
		}

		.zoom img {
			display: block;
		}

		.zoom img::selection { background-color: transparent; }

		#ex2 img:hover { cursor: url(grab.cur), default; }
		#ex2 img:active { cursor: url(grabbed.cur), default; }
</style>
@endpush




    


