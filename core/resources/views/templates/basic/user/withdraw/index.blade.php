@extends($activeTemplate . 'user.layouts.app')
@section('panel')
{{-- @dump($dinaran) --}}
    <div class="row  mt-2">
        @if (isset($dinaran))
            <div class="col-xl-4 col-xxl-3 col-lg-6 col-md-5 col-sm-6 mb-4">
                <div class="card card-deposit method-card">
                    <h5 class="card-header text-center">Withdraw to Dinaran</h5>
                    <div class="card-body card-body-deposit">
                        {{-- <img src="{{getImage(imagePath()['withdraw']['method']['path'].'/'. $data->image)}}"
                                class="card-img-top w-100" alt="{{__($data->name)}}"> --}}
                            <div class="deposit-content">
                                <h5>Dinaran Account</h5>
                                <ul  class="text-center font-15 ">
                                    <li>
                                        @lang('Name')
                                        : <span> {{$dinaran->name}}</span>
                                    </li>
                                    <li>
                                        @lang('Email')
                                        : <span> {{$dinaran->email}}</span>
                                    </li>
                                    <li>
                                        @lang('Phone')
                                        : <span> {{$dinaran->phone}}</span>
                                    </li>
                                </ul>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="javascript:void(0)"  
                            class="btn btn-block  btn--success deposit" data-toggle="modal" data-target="#exampleModal2">
                            @lang('Withdraw Now')</a>
                    </div>
                </div>
            </div>
        @else
            <div class="col-xl-4 col-xxl-3 col-lg-6 col-md-5 col-sm-6 mb-4">
                <div class="card card-deposit method-card">
                    <h5 class="card-header text-center">Connect Dinaran Account</h5>
                    <div class="card-footer">
                        <a href="javascript:void(0)" 
                            class="btn btn-block  btn--success deposit" data-toggle="modal" data-target="#exampleModal">
                            @lang('Connect')</a>
                    </div>
                </div>
            </div>
            @endif

        </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title method-name" id="exampleModalLabel">Register Dinaran Account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('user.dinaran.store')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Name</label>
                            <input class="form-control" type="text" name="name" id="name"  placeholder="name"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input class="form-control" type="email" name="email" id="email"  placeholder="email"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="">Phone</label>
                            <input class="form-control" type="text" name="phone" id="phone"  placeholder="phone"
                                required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--secondary" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn--success">@lang('Confirm')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title method-name" id="exampleModalLabel">Withdraw to Dinaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('user.dinaran.withdraw')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="phone"  class="edit-currency form-control" value="{{$dinaran->phone ?? ''}}">
                        </div>
                        <div class="form-group">
                            <label>@lang('Enter Amount'):</label>
                            <div class="input-group">
                                <input id="amount" type="text" class="form-control form-control-lg" onkeyup="this.value = this.value.replace (/^\.|[^\d\.]/g, '')" name="amount" placeholder="0.00" required=""  value="{{old('amount')}}">
                                <div class="input-group-append">
                                    <span class="input-group-text currency-addon">{{__($general->cur_text)}}</span>
                                </div>
                            </div>
                        </div>
                        <p class="text-danger text-center depositLimit"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--secondary" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn--success">@lang('Confirm')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        'use strict';
        (function($){
            $('.deposit').on('click', function () {
            });
        })(jQuery)
    </script>
@endpush

