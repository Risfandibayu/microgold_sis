@extends($activeTemplate . 'user.layouts.app')

@section('panel')
    <div class="row">
        {{-- @dump(intval($claimbro / 3600000) > auth()->user()->bro_claim) --}}
        @if (intval($claimbro / 3600000) > auth()->user()->bro_claim)
        <div class="col-xl-3 col-lg-4 col-sm-6 mb-30 text-center">
            <div class="dashboard-w1  h-100 w-100 bg--3 b-radius--10 box-shadow">
                <div class="icon">
                    <i class="las la-user"></i>
                </div>
                <div class="details">
                    <div class="numbers" >
                        <span class="amount">{{intval($claimbro / 3600000) - auth()->user()->bro_claim}}</span>
                        <span class="currency-sign">BRO</span>
                    </div>
                    <div class="desciption">
                        <span class="text--small">BRO Available To Claim</span>
                    </div>
                </div>
                <br>
                <button class="btn btn-sm btn-block text--small bg--white text--black box--shadow3 mt-3 claim2"
                    data-max="{{intval($claimbro / 3600000) - auth()->user()->bro_claim}}">@lang('Claim SP')</button>
            </div>
        </div>
        @endif


        <div class="col-lg-12">
            <div class="card b-radius--10 ">
                <div class="card-body p-0">
                    <div class="table-responsive--sm table-responsive">
                        <table class="table table--light style--two">
                            <thead>
                            <tr>
                                <th scope="col">@lang('SL')</th>
                                <th scope="col">@lang('Date')</th>
                                <th scope="col">@lang('TRX')</th>
                                <th scope="col">@lang('Amount')</th>
                                <th scope="col">@lang('Charge')</th>
                                <th scope="col">@lang('Post Balance')</th>
                                <th scope="col">@lang('Post Balance On Hold')</th>
                                <th scope="col">@lang('Detail')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($transactions  as $trx)
                                <tr>
                                    <td data-label="@lang('SL')">{{ $transactions->firstItem()+$loop->index }}</td>
                                    <td data-label="@lang('Date')">{{ showDateTime($trx->created_at) }}</td>
                                    <td data-label="@lang('TRX')" class="font-weight-bold">{{ $trx->trx }}</td>
                                    <td data-label="@lang('Amount')" class="budget">
                                        <strong @if($trx->trx_type == '+') class="text-success"
                                                @else class="text-danger" @endif> {{($trx->trx_type == '+') ? '+':'-'}} {{nb(getAmount($trx->amount))}} {{$general->cur_text}}</strong>
                                    </td>
                                    <td data-label="@lang('Charge')"
                                        class="budget">{{ $general->cur_sym }} {{ nb(getAmount($trx->charge)) }} </td>
                                    <td data-label="@lang('Post Balance')">{{ nb($trx->post_balance +0) }} {{$general->cur_text}}</td>
                                    <td data-label="@lang('Post Balance')">{{ nb($trx->post_balance_on_hold +0) }} {{$general->cur_text}}</td>
                                    <td data-label="@lang('Detail')">{{ __($trx->details) }}
                                    {{-- @if ($trx->remark == "purchased_product" && $trx->amount>=3600000 && $trx->is_claim == 0)
                                    <button class="btn btn--warning btn-sm claim"
                                    data-id="{{ $trx->id }}"
                                    data-max="{{intval($trx->amount / 3600000)}}"
                                    ><i class="fa fa-exclamation" aria-hidden="true"></i> Claim {{intval($trx->amount / 3600000)}} BRO</button>
                                    @endif --}}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer py-4">
                    {{ $transactions->appends($_GET)->links() }}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="claimbro">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('Claim BRO Confirmation')</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('user.claimbro')}}">
                    {{-- <form method="post"> --}}
                        {{-- <div class="modal-body"> --}}
                            {{-- </div> --}}
                        @csrf
                <div class="modal-body">
                    <h5><span class="">@lang('When you claim BRO, you have to pay an admin fee of') <b> @lang('650.000 IDR')</b>
                            @lang('/ BRO').</span>
                        <br>
                        
                    </h5>
                    <input type="hidden" class="id" name="trxid" id="">
                            <div class="form-group">
                                <label for="">BRO Qty will be claim :</label>
                                <input class="form-control qty" type="number" name="qty" id="qty" min="1" value="" max="" placeholder="BRO qty" required>
                            </div>
                    <h5>
                        <span class=""> @lang('Are you sure to proceed the claim?')</span>
                    </h5>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Close')</button>
                    <button type="submit" class="btn btn--success"><i
                        class="lab la-telegram-plane"></i> @lang('Confirm')</button>
                </div>
            </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="claimbro2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('Claim BRO Confirmation')</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('user.claimbro2')}}">
                    {{-- <form method="post"> --}}
                        {{-- <div class="modal-body"> --}}
                            {{-- </div> --}}
                        @csrf
                <div class="modal-body">
                    <h5><span class="">@lang('When you claim BRO, you have to pay an admin fee of') <b> @lang('650.000 IDR')</b>
                            @lang('/ SP').</span>
                        <br>
                        
                    </h5>
                    <input type="hidden" class="id" name="trxid" id="">
                            <div class="form-group">
                                <label for="">BRO Qty will be claim :</label>
                                <input class="form-control qty" type="number" name="qty" id="qty" min="1" value="" max="" placeholder="BRO qty" required>
                            </div>
                    <h5>
                        <span class=""> @lang('Are you sure to proceed the claim?')</span>
                    </h5>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Close')</button>
                    <button type="submit" class="btn btn--success"><i
                        class="lab la-telegram-plane"></i> @lang('Confirm')</button>
                </div>
            </form>
            </div>
        </div>
    </div>

@endsection


@push('breadcrumb-plugins')
    <form action="" method="GET" class="form-inline float-sm-right bg--white">
        <div class="input-group has_append">
            <input type="text" name="search" class="form-control" placeholder="@lang('Search by TRX')" value="{{ $search ?? '' }}">
            <div class="input-group-append">
                <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
@endpush

@push('script')
    <script>
        'use strict';
        (function($){
            $("select[name=country]").val("{{ auth()->user()->address->country }}");


            $('.claim').on('click', function () {
                var modal = $('#claimbro');
                modal.find('.id').val($(this).data('id'));
                modal.find('.qty').prop('max',$(this).data('max'));
                modal.find('.qty').prop('value',$(this).data('max'));
                modal.modal('show');
            });
            $('.claim2').on('click', function () {
                var modal = $('#claimbro2');
                modal.find('.qty').prop('max',$(this).data('max'));
                modal.find('.qty').prop('value',$(this).data('max'));
                modal.modal('show');
            });



        })(jQuery)

        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };


    </script>
@endpush
