<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Muhamad Nauval Azhar">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="description" content="This is a login page template based on Bootstrap 5">
	<title> {{ $general->sitename}} - {{__(@$page_title)}} </title>
    <link rel="shortcut icon" href="{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/global/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/global/css/line-awesome.min.css')}}">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link
    href="https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    rel="stylesheet">

    <link href="{{asset('assets/new_landing2')}}/css/auth.css" rel="stylesheet">
</head>

<body>
	<section class="h-100">
		<div class="container h-100">
			<div class="row justify-content-sm-center h-100">
				<div class="col-xxl-4 col-xl-5 col-lg-5 col-md-7 col-sm-9">
					<div class="text-center my-5">
						<img src="{{getImage(imagePath()['logoIcon']['path'] .'/logo.png')}}"
                        alt="@lang('site-logo')" width="300">
					</div>
					<div class="card shadow-sm" style="border-radius: 3%">
						<div class="card-body p-4">
							<h1 class="fs-4 card-title text-center fw-bold mb-4">Log in</h1>
							<form method="POST" class="needs-validation" novalidate="" autocomplete="off" action="{{route('user.login')}}">
                                @csrf
								<div class="mb-3">
									<label class="mb-2 " for="email">Akun</label>
									<input id="email" type="text" class="form-control" name="username" value="{{old('username')}}" required autofocus placeholder="Username/E-mail/BRO Number">
									<div class="invalid-feedback">
										Username is invalid
									</div>
								</div>

								<div class="mb-3">
									<div class="mb-2 w-100">
										<label class="" for="password">Password</label>
										
									</div>
                                    <div class="input-group" id="show_hide_password">
                                        <input class="form-control form--control-2 " id="myInputThree" name="password"
                                            placeholder="@lang('Password')" type="password">
                                        <div class="input-group-text form--control-2">
                                            <a class=""><i class="text-dark fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                    </div>

								    <div class="invalid-feedback">
								    	Password is required
							    	</div>
								</div>
                                <div class="mb-4">
                                        <a href="{{ route('user.password.request') }}" class="float-end">
                                            Lupa Password?
                                        </a>
                                </div>
                                <br>

								<div class="d-flex align-items-center row justify-content-center mt-2">
                                    <div class="col-12 row">
                                        <button type="submit" class="btn btn-block btn-primary ms-auto btn-login">
                                            Log in
                                        </button>
                                    </div>
								</div>
							</form>
						</div>
					</div>
					<div class="text-center mt-3 text-muted">
						<span class="belum">Belum Punya Akun?</span> <a href="{{ route('user.register') }}">Daftar Sekarang</a> 
					</div>
				</div>
			</div>
		</div>
	</section>

	<script src="{{asset('assets/new_landing2')}}/js/auth.js"></script>
    <script src="{{asset('assets/global/js/jquery-3.6.0.min.js')}}"></script>
    @include('partials.notify')
    <script>
         $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password input').attr("type") == "text"){
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass( "fa-eye" );
                    $('#show_hide_password i').removeClass( "fa-eye-slash" );
                }else if($('#show_hide_password input').attr("type") == "password"){
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "fa-eye" );
                    $('#show_hide_password i').addClass( "fa-eye-slash" );
                }
            });
        });
    </script>
</body>
</html>