@extends($activeTemplate . 'user.layouts.app')

@section('panel')
    <div class="container">
        <div class="row  justify-content-center">
            <div class="col-md-12 col-lg-12 col-xl-10 col-xxl-7">
                <div class="card card-deposit text-center">
                    <div class="card-body card-body-deposit">
                        <div class="py-4">
                            {{-- @dump($depo) --}}
                            <span>Please make a transfer to the following {{$depo->bank}} virtual account number:</span>
                            <br>
                            {{-- <h4>{{$depo->bank}}</h4> --}}
                            <div class="input-group mt-3 mb-3">
                                <input type="text" class="form-control form-control-lg" id="copy-input" value="{{$depo->payment}}" placeholder="VA" aria-label="VA" aria-describedby="basic-addon2" disabled style="background-color: white;
                                font-size: 20px!important;
                                /* align-content: center; */
                                /* text-align: center; */
                                border: 2px dashed #383838;">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <button class="btn btn-sm" onclick="myFunction()" type="button" id="copy-button"
                                            data-placement="button"
                                            title="Copy to Clipboard">
                                            Copy
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <h4>{{$depo->payment_name}}</h4>
                            <p>Amount</p>
                            <h4>Rp. {{nb($depo->total)}}</h4>
                            <br>
                            <p>Payment Deadline</p>
                            {{-- <h4>{{$depo->expired}}</h4> --}}
                            <h4 id="demo"></h4>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@stop

@push('script')
<script>
    
        function myFunction() {
  // Get the text field
            var copyText = document.getElementById("copy-input");

            // Select the text field
            copyText.select();
            copyText.setSelectionRange(0, 99999); // For mobile devices

            // Copy the text inside the text field
            navigator.clipboard.writeText(copyText.value);

            // Alert the copied text
            // alert("Copied the text: " + copyText.value);
            iziToast['success']({
                message: "Copied the text: " + copyText.value,
                position: "topRight"
            });
        }
</script>

<script>
    // Set the date we're counting down to
    var countDownDate = new Date("{{$depo->expired}}").getTime();
    
    // Update the count down every 1 second
    var x = setInterval(function() {
    
      // Get today's date and time
      var now = new Date().getTime();
        
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
        
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
      // Output the result in an element with id="demo"
      document.getElementById("demo").innerHTML = days + "d " + hours + "h "
      + minutes + "m " + seconds + "s ";
        
      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
      }
    }, 1000);
    </script>
@endpush

