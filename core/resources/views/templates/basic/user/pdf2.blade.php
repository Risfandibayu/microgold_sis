@extends($activeTemplate . 'user.layouts.app')

@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">
@endpush

@section('panel')
<div class="row">
    <div class="col-lg-12">
        <iframe src="" name="myIframe" style="border:0px #ffffff none;" name="myiFrame" scrolling="no" frameborder="1"
            marginheight="0px" marginwidth="0px" height="700px" width="100%" allowfullscreen></iframe>
    </div>
</div>
<form target="myIframe" id="form" action="" method="post">
    @csrf
</form>
@endsection
@push('script')
<script>
    $(document).ready(function(){
        var action = "{{$url}}"; // Ganti dengan URL baru
        $("#form").attr("action", action);
        console.log(action);
        $("#form").submit();
        var newAction = "baru.php"; // Ganti dengan URL baru
        $("#form").attr("action", newAction);
    });
</script>
@endpush

@push('breadcrumb-plugins')
<a href="{{route('user.pdf2',[1])}}" class="btn btn-sm btn--success custom-order"><i
        class="fa fa-fw fa-book"></i>@lang('Ebook 1')</a>
<a href="{{route('user.pdf2',[2])}}" class="btn btn-sm btn--success custom-order"><i
        class="fa fa-fw fa-book"></i>@lang('Ebook 2')</a>
<a href="{{route('user.pdf2',[3])}}" class="btn btn-sm btn--success custom-order"><i
        class="fa fa-fw fa-book"></i>@lang('Ebook 3')</a>
@endpush