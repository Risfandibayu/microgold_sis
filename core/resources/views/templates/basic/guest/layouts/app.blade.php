@extends($activeTemplate .'guest.layouts.master')

@section('content')
    <div class="page-wrapper default-version">
        @include($activeTemplate .'guest.partials.sidenav')
        @include($activeTemplate .'guest.partials.topnav')
        <div class="body-wrapper">
            <div class="bodywrapper__inner">
                @include($activeTemplate .'guest.partials.breadcrumb')
                @yield('panel')
            </div>
        </div>
    </div>
@endsection
